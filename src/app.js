const dotenv = require('dotenv');
const mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;  
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const express = require("express");
const bodyParser = require('body-parser');
var path = require('path');
// const port = 3000;
const app = express();
var destPath = __dirname + "\\public\\";
app.use(express.static(path.join(__dirname, 'public')));
const logger = require('./exception/logger');


app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
  next();
});

//parsing middlewares
app.use(bodyParser.json({limit:'50mb',extended:true}));
app.use(bodyParser.urlencoded({limit:'100mb',extended:true}));


var conn = mongoose.createConnection(process.env.MONGO_URI);

conn.on('connected', () => {
    console.log('connected to smart_booking database');
});
  
conn.on('disconnected', () => {
    console.log('connection disconnected smart_booking database');
});

app.get('/', (req, res) => {
    res.send('Hello World!')
  });


function logErrors (err, req, res, next) {
  logger.error(err)
  next(err);
}

function errorHandler (err, req, res, next) {
  res.status(500).send({ error: 'Something went wrong.' })
}

app.get('/errorhandler', (req, res, next) => {
  try {
    throw new Error('Wowza!')
  } catch (error) {
    next(error)
  }
})
var auth = require('./routes/auth');
var user = require('./routes/user_routes');
var customer = require('./routes/address_master_routes');
var pincodePerformance = require('./routes/pincode_performance_routes');
var orderHistory = require('./routes/order_history_routes');
var orderStatus = require('./routes/order_status_routes');
var paytype = require('./routes/paytype_routes');
var deliveryPossibility = require('./routes/delivery_possibility_routes');
var flag = require('./routes/flag_routers');
var recommendation = require('./routes/recommendation_routes')
var pincodeZones = require('./routes/pincode_zones_routes')




// routes for common controllers
app.use(`/apis`, auth);
app.use(`/apis`,user);
app.use(`/apis`,customer);
app.use(`/apis`,pincodePerformance);
app.use(`/apis`,orderHistory);
app.use(`/apis`,orderStatus);
app.use(`/apis`,paytype);
app.use(`/apis`,deliveryPossibility);
app.use(`/apis`,flag);
app.use(`/apis`,recommendation);
app.use(`/apis`,pincodeZones);





// error handler middelware
app.use(logErrors)
app.use(errorHandler)


  // server listening 
const port = process.env.API_PORT
app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
module.exports = app;