const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});

module.exports = {

    HOST: process.env.LANDMARK_HOST,
    
    USER: process.env.LANDMARK_USER,
    
    PASSWORD: process.env.LANDMARK_PASSWORD,
    
    DB: process.env.LANDMARK_DB_NAME,
    
    dialect: "mysql",
    
    pool: {
    
    max: 5,
    
    min: 0,
    
    acquire: 30000,
    
    idle: 10000
    
    }
    
    };