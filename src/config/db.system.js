const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});

module.exports = {

    HOST: process.env.SYSTEM_HOST,
    
    USER: process.env.SYSTEM_USER,
    
    PASSWORD: process.env.SYSTEM_PASSWORD,
    
    DB: process.env.SYSTEM_DB_NAME,
    
    dialect: "mysql",
    
    pool: {
    
    max: 5,
    
    min: 0,
    
    acquire: 30000,
    
    idle: 10000
    
    }
    
    };