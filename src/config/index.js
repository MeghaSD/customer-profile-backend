const dbLandmark = require("./db.landmark");
const dbSystem = require("./db.system");
const Sequelize = require("sequelize");

const sequelizeOne = new Sequelize(dbLandmark.DB, dbLandmark.USER, dbLandmark.PASSWORD, {
  host: dbLandmark.HOST,
  dialect: dbLandmark.dialect,
  operatorsAliases: false,

  pool: {
    max: dbLandmark.pool.max,
    min: dbLandmark.pool.min,
    acquire: dbLandmark.pool.acquire,
    idle: dbLandmark.pool.idle
  }
});


const sequelizeTwo = new Sequelize(dbSystem.DB, dbSystem.USER, dbSystem.PASSWORD, {
  host: dbSystem.HOST,
  dialect: dbSystem.dialect,
  operatorsAliases: false,

  pool: {
    max: dbSystem.pool.max,
    min: dbSystem.pool.min,
    acquire: dbSystem.pool.acquire,
    idle: dbSystem.pool.idle
  }
});

const landmark = {};


landmark.Sequelize = Sequelize;
landmark.sequelize = sequelizeOne;
landmark.pincode_zones = require("../models/pincode_zones")(sequelizeOne, Sequelize);
landmark.pincode_zones_date = require("../models/pincode_zones_date")(sequelizeOne, Sequelize);
landmark.pickup = require("../models/pickup")(sequelizeOne, Sequelize);


const system = {};

system.Sequelize = Sequelize;
system.sequelize = sequelizeTwo;
system.ilogix_standard_pincodes_master = require("../models/ilogix_standard_pincodes_master")(sequelizeTwo, Sequelize);
system.consignor_standard_billing_zones_alias = require("../models/consignor_standard_billing_zones_alias")(sequelizeTwo, Sequelize);
system.ilogix_standard_other_pincodes = require("../models/ilogix_standard_other_pincodes")(sequelizeTwo, Sequelize);
module.exports = {landmark, system};
