const addressModel = require('../models/address_masters');
const customerTypeModel = require('../models/customer_type_masters');
const customerHistoryModel = require('../models/customer_history_masters');
const zoneModel = require('../models/zone_masters');
var trim = require('trim');
exports.saveAddress = async (req, res, next) => {
    if (req.body.address_label && (req.body.address_label !== undefined || req.body.address_label !== 'undefined' || req.body.address_label !== '') && req.body.address_value && (req.body.address_value !== undefined || req.body.address_value !== 'undefined' || req.body.address_value !== '')) {
        var address_label = trim(req.body.address_label)
        var address_value = trim(req.body.address_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await addressModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    addressModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                address_label: address_label,
                                address_value: address_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Address Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Address Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var addressData = new addressModel({
                address_label: address_label,
                address_value: address_value
            });
            addressData.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Address Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Address Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getCustomerTypes = async (req, res, next) => {
    try {
        const customerTypes = await customerTypeModel.find({});
        if (customerTypes != '') {
            res.json({
                status: 200,
                message: 'success',
                data: customerTypes,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: customerTypes
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}

exports.getCustomerHistory = async (req, res, next) => {
    try {
        if(req.query.type &&(req.query.type != undefined || req.query.type != 'undefined' || req.query.type != null)){
            var type = trim(req.query.type);
            var customerHistory = [];
            if(type == 'New'){
                customerHistory = await customerHistoryModel.find({
                    customer_history_label: 'Not Available'
                });
            }else{
                customerHistory = await customerHistoryModel.find({
                    customer_history_label: 'Available'
                });
            }
           
        if (customerHistory != '') {
            res.json({
                status: 200,
                message: 'success',
                data: customerHistory,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: customerHistory
            })
        }
        }else{
            res.json({
                status:401,
                message: 'Field is required'
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}

exports.getZones = async (req, res, next) => {
    try {
        const zones = await zoneModel.find({});
        if (zones != '') {
            res.json({
                status: 200,
                message: 'success',
                data: zones,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: zones
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}

exports.getAddressMasters = async (req, res, next) => {
    try {
        const address = await addressModel.find({});
        if (address != '') {
            res.json({
                status: 200,
                message: 'success',
                data: address,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: address
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}