const { promisify } = require("util");
const jwt = require("jsonwebtoken");
const usersModel = require('../models/users');
var trim = require('trim');


const createToken = email => {
    return jwt.sign(
        {
            email,
        },
        process.env.JWT_SECRET,
        {
            expiresIn: process.env.JWT_EXPIRES_IN,
        },
    );
};

exports.login = async (req, res, next) => {
    // console.log("reached login", req.body)
    try {
        // 1) check if email and password exist
        if (!req.body.email || !req.body.password) {
            res.json({
                status: 404,
                message: "fail, Please provide email or password"
            })
        } else {
            // 2) check if user exist and password is correct
            var email = trim(req.body.email)
            var password = trim(req.body.password)
            const user = await usersModel.findOne({
                email,
                password
            });
            if (!user) {
                res.json({
                    status: 401,
                    message: "fail, Email or Password is wrong"
                })
            } else {
                // 3) All correct, send jwt to client
                const token = createToken(user.email);
                // Remove the password from the output
                user.password = undefined;
                user.passwordHash = undefined;
                user.salt = undefined;
                res.status(200).json({
                    status: 200,
                    message: 'success',
                    token,
                    data: {
                        user,
                    },
                });
            }
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
};

/*
    protect middleware to all apis  (Date: 18-04-2022)
*/
exports.protect = async (req, res, next) => {
    try {
        // 1) check if the token is there
        let token
        if (req.headers.authorization && req.headers.authorization.startsWith("Bearer")) {
            token = req.headers.authorization.split(" ")[1];
        }
        if (!token) {
            res.json({
                status: 401,
                message: 'No token provided'
            })
        } else {

            // 2) Verify token
            const decode = await promisify(jwt.verify)(token, process.env.JWT_SECRET,);

            // 3) check if the user is exist (not deleted)
            const user = await usersModel.find({ "email": decode.email });
            if (user == '') {
                res.json({
                    status: 401,
                    message: 'Invalid token'
                })
            } else {
                req.user = user;
                next();
            }
        }
    } catch (error) {
        return res.status(401).json({
            message: 'Token Expired',
        })
    }
};