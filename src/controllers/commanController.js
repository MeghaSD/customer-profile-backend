const ilogix_standard_pincodes_master = require("../dao/ilogix_standard_pincode_dao");
const consignor_standard_billing_zones_alias = require("../dao/consignor_standard_zones_dao");
const ilogix_standard_other_pincodes = require("../dao/ilogix_other_pincode_dao");
const trim = require('trim');
const recommendation_config = require("../models/recommendation");
const customer_performance_model = require("../models/customer_performance");
const deliveryPossibilityModel = require('../models/delivery_possibility_masters');
const flagModel = require('../models/flag_masters');
const pincode_performance_date_model = require("../models/pincode_performance_date");
const customer_performance_date_model = require("../models/customer_performance_date");

exports.getZonesData = async (pickupPincode, deliveryPincode, airwaybilno, id, telephone1, dispatch_status, product, consignee_email, consignee_address1, product_value, collectable_value) => {
    let pincode_zones_data = [];
    let zone_code = '';
    let zone = '';
    if(consignee_address1 != null){
        var address = trim(consignee_address1.replace(/[^a-zA-Z0-9]/g, "").toLowerCase())
    }else{
        var address = null
    }
    // console.log("data---",pickupPincode,deliveryPincode,id,telephone1,dispatch_status,product,consignee_email,product_value,collectable_value)
    var pincode_master = await ilogix_standard_pincodes_master.getIlogixStandardPincodes(pickupPincode, deliveryPincode)

    if (pincode_master && (pincode_master != '' && pincode_master.length >= 2)) {
        // console.log("pincode_master--",pincode_master)
        var pickupCity = pincode_master.find(x => x.pincode === pickupPincode).city_id;
        //  console.log("pickupCity----",pickupCity)
        var deliveryCity = pincode_master.find(x => x.pincode === deliveryPincode).city_id;
        // console.log("deliveryCity---",deliveryCity)
        var pickupState = pincode_master.find(x => x.pincode === pickupPincode).state_id;
        //  console.log("pickupState----",pickupState)
        var deliveryState = pincode_master.find(x => x.pincode === deliveryPincode).state_id;
        if (pickupCity && deliveryCity && pickupCity.toLowerCase() === deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
            zone_code = 'IC';
            var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
            if (consignor_zone_data != '') {
                console.log("Intra City @@@@", consignor_zone_data)
                zone = consignor_zone_data[0].zone;
                await pincode_zones_data.push({
                    airwaybilno: airwaybilno,
                    zone: zone,
                    pickup_id: id,
                    mobileno: telephone1,
                    dispatch_status: dispatch_status,
                    pay_type: product,
                    delivery_pincode: deliveryPincode,
                    email_id: consignee_email,
                    address: address,
                    product_value: product_value,
                    collect_value: collectable_value
                })
            }
        } else if (pickupCity && deliveryCity && pickupCity.toLowerCase() != deliveryCity.toLowerCase() && pickupState.toLowerCase() === deliveryState.toLowerCase()) {
            zone_code = 'IS';
            var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
            if (consignor_zone_data != '') {
                zone = consignor_zone_data[0].zone;
                await pincode_zones_data.push({
                    airwaybilno: airwaybilno,
                    zone: zone,
                    pickup_id: id,
                    mobileno: telephone1,
                    dispatch_status: dispatch_status,
                    pay_type: product,
                    delivery_pincode: deliveryPincode,
                    email_id: consignee_email,
                    address: address,
                    product_value: product_value,
                    collect_value: collectable_value
                })
            }
        }

    }

    console.log("zone---", zone)
    if (zone == '') {
        if (pincode_zones_data.length == 0) {
            // NJ Special Zone check
            console.log("nj zone checked.....")
            var zone_main = ['IC', 'IS'];
            var zone_code1 = '';
            const found = zone_main.some(r => zone.includes(r))
            if (zone == '' || found === false) {
                var customList = await consignor_standard_billing_zones_alias.customDeliveryList()
                // console.log("customList---",customList)
                for (let czone of customList) {
                    zone_code1 = czone.zone_code;
                }
                var otherPincodeZone = await ilogix_standard_other_pincodes.getOtherPincodeZone(zone_code1, deliveryPincode)
                // console.log("otherPincodeZone---",otherPincodeZone)
                if (otherPincodeZone != '') {
                    var custom_pickup_pincodes = [];
                    for (let cpincode of otherPincodeZone) {
                        custom_pickup_pincodes.push(cpincode.pincode)
                    }
                    const found1 = custom_pickup_pincodes.some(r => deliveryPincode.includes(r))
                    if (found1 !== false) {
                        console.log("otherPincodeZone---@@@@@@@")
                        zone_code = zone_code1;
                        var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                        if (consignor_zone_data != '') {
                            zone = consignor_zone_data[0].zone;
                            await pincode_zones_data.push({
                                airwaybilno: airwaybilno,
                                zone: zone,
                                pickup_id: id,
                                mobileno: telephone1,
                                dispatch_status: dispatch_status,
                                pay_type: product,
                                delivery_pincode: deliveryPincode,
                                email_id: consignee_email,
                                address: address,
                                product_value: product_value,
                                collect_value: collectable_value
                            })
                        }
                    }
                } else {
                    pincode_zones_data = []
                }


            }
        }

        if (pincode_zones_data.length == 0) {
            // West zone check
            var westZone = [{ zone_code: 'WZ' }];
            var zone_code2 = '';
            for (let czone of westZone) {
                zone_code2 = czone.zone_code
            }
            var westZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code2, pickupPincode, deliveryPincode)
            if (westZoneData != '') {
                // console.log("reached on west zone @@@@@@--",westZoneData)
                var westPincode = [];
                for (let cpincode of westZoneData) {
                    westPincode.push(cpincode.pincode)
                }
                const found2 = westPincode.some(r => pickupPincode.includes(r))
                const found3 = westPincode.some(r => deliveryPincode.includes(r))
                if (found2 !== false && found3 !== false) {
                    zone_code = zone_code2
                    console.log("westZone---@@@@@@@")
                    var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                    if (consignor_zone_data != '') {
                        zone = consignor_zone_data[0].zone;
                        await pincode_zones_data.push({
                            airwaybilno: airwaybilno,
                            zone: zone,
                            pickup_id: id,
                            mobileno: telephone1,
                            dispatch_status: dispatch_status,
                            pay_type: product,
                            delivery_pincode: deliveryPincode,
                            email_id: consignee_email,
                            address: address,
                            product_value: product_value,
                            collect_value: collectable_value
                        })
                    }
                }

            } else if (westZoneData.length < 2 || westZoneData == '') {
                pincode_zones_data = []
            }

        }

        if (pincode_zones_data.length == 0) {
            // East zone check
            var eastZone = [{ zone_code: 'EZ' }]
            var zone_code3 = '';
            for (let czone of eastZone) {
                zone_code3 = czone.zone_code
            }
            var eastZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code3, pickupPincode, deliveryPincode)
            if (eastZoneData != '') {
                var eastPincode = [];
                for (let cpincode of eastZoneData) {
                    eastPincode.push(cpincode.pincode)
                }
                const found4 = eastPincode.some(r => pickupPincode.includes(r))
                const found5 = eastPincode.some(r => deliveryPincode.includes(r))
                if (found4 !== false && found5 !== false) {
                    zone_code = zone_code3;
                    console.log("eastzone---@@@@@@@")
                    var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                    if (consignor_zone_data != '') {
                        zone = consignor_zone_data[0].zone;
                        await pincode_zones_data.push({
                            airwaybilno: airwaybilno,
                            zone: zone,
                            pickup_id: id,
                            mobileno: telephone1,
                            dispatch_status: dispatch_status,
                            pay_type: product,
                            delivery_pincode: deliveryPincode,
                            email_id: consignee_email,
                            address: address,
                            product_value: product_value,
                            collect_value: collectable_value
                        })
                    }
                }
            } else if (eastZoneData.length < 2 || eastZoneData == '') {
                pincode_zones_data = []
            }
        }

        if (pincode_zones_data.length == 0) {
            // North zone check
            var northZone = [{ zone_code: 'NZ' }];
            var zone_code4 = ''
            for (let czone of northZone) {
                zone_code4 = czone.zone_code
            }
            var northZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code4, pickupPincode, deliveryPincode)
            if (northZoneData != '') {
                var northPincode = [];
                for (let cpincode of northZoneData) {
                    northPincode.push(cpincode.pincode)
                }
                const found6 = northPincode.some(r => deliveryPincode.includes(r))
                const found7 = northPincode.some(r => deliveryPincode.includes(r))
                if (found6 !== false && found7 !== false) {
                    zone_code = zone_code4
                    console.log("northZone---@@@@@@@")
                    var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                    if (consignor_zone_data != '') {
                        zone = consignor_zone_data[0].zone;
                        await pincode_zones_data.push({
                            airwaybilno: airwaybilno,
                            zone: zone,
                            pickup_id: id,
                            mobileno: telephone1,
                            dispatch_status: dispatch_status,
                            pay_type: product,
                            delivery_pincode: deliveryPincode,
                            email_id: consignee_email,
                            address: address,
                            product_value: product_value,
                            collect_value: collectable_value
                        })
                    }
                }


            } else if (northZoneData.length < 2 || northZoneData == '') {
                pincode_zones_data = []
            }

        }

        if (pincode_zones_data.length == 0) {
            //South zone check
            var southZone = [{ zone_code: 'SZ' }];
            var zone_code5 = '';
            for (let czone of southZone) {
                zone_code5 = czone.zone_code
            }
            var southZoneData = await ilogix_standard_other_pincodes.getOtherPincodeZone2(zone_code5, pickupPincode, deliveryPincode)
            if (southZoneData != '') {
                var southPincode = [];
                for (let cpincode of southZoneData) {
                    southPincode.push(cpincode.pincode)
                }
                const found8 = southPincode.some(r => pickupPincode.includes(r))
                const found9 = southPincode.some(r => deliveryPincode.includes(r))
                if (found8 !== false && found9 !== false) {
                    zone_code = zone_code5
                    console.log("southZone---@@@@@@@")
                    var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                    if (consignor_zone_data != '') {
                        zone = consignor_zone_data[0].zone;
                        await pincode_zones_data.push({
                            airwaybilno: airwaybilno,
                            zone: zone,
                            pickup_id: id,
                            mobileno: telephone1,
                            dispatch_status: dispatch_status,
                            pay_type: product,
                            delivery_pincode: deliveryPincode,
                            email_id: consignee_email,
                            address: address,
                            product_value: product_value,
                            collect_value: collectable_value
                        })
                    }
                }

            } else if (southZoneData.length < 2 || southZoneData == '') {
                pincode_zones_data = []
            }
        }

        if (pincode_zones_data.length == 0) {
            // Metro to Metro zone check
            var metroZone = [{ zone_code: 'MM' }];
            var zone_code6 = '';
            for (let czone of metroZone) {
                zone_code6 = czone.zone_code
            }
            var metroZoneData = await ilogix_standard_other_pincodes.getMetroZone(zone_code6, pickupPincode, deliveryPincode)
            if (metroZoneData != '') {
                var metroPincode = [];
                for (let cpincode of metroZoneData) {
                    metroPincode.push(cpincode.pincode)
                }
                const found10 = metroPincode.some(r => pickupPincode.includes(r))
                const found11 = metroPincode.some(r => deliveryPincode.includes(r))
                if (found10 !== false && found11 !== false) {
                    zone_code = zone_code6
                    console.log("metroZone---@@@@@@@")
                    var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                    if (consignor_zone_data != '') {
                        zone = consignor_zone_data[0].zone;
                        await pincode_zones_data.push({
                            airwaybilno: airwaybilno,
                            zone: zone,
                            pickup_id: id,
                            mobileno: telephone1,
                            dispatch_status: dispatch_status,
                            pay_type: product,
                            delivery_pincode: deliveryPincode,
                            email_id: consignee_email,
                            address: address,
                            product_value: product_value,
                            collect_value: collectable_value
                        })
                    }
                }
            } else if (metroZoneData.length < 2 || metroZoneData == '') {
                pincode_zones_data = []
            }
        }

        if (pincode_zones_data.length == 0) {
            // Rest of India zone check
            var restOfIndiaZone = [{ zone_code: 'ROI' }];
            var zone_code7 = '';
            for (let czone of restOfIndiaZone) {
                zone_code7 = czone.zone_code
            }
            var restOfIndiaZoneData = await ilogix_standard_other_pincodes.getRestOfIndiaZone(zone_code7, deliveryPincode)
            if (restOfIndiaZoneData != '') {
                var restOfIndiaPincode = [];
                for (let cpincode of restOfIndiaZoneData) {
                    restOfIndiaPincode.push(cpincode.pincode)
                }
                const found12 = restOfIndiaPincode.some(r => deliveryPincode.includes(r))
                if (found12 !== false) {
                    zone_code = zone_code7
                    console.log("restOfIndiaZone---@@@@@@@")
                    var consignor_zone_data = await consignor_standard_billing_zones_alias.getZone(zone_code)
                    if (consignor_zone_data != '') {
                        zone = consignor_zone_data[0].zone;
                        await pincode_zones_data.push({
                            airwaybilno: airwaybilno,
                            zone: zone,
                            pickup_id: id,
                            mobileno: telephone1,
                            dispatch_status: dispatch_status,
                            pay_type: product,
                            delivery_pincode: deliveryPincode,
                            email_id: consignee_email,
                            address: address,
                            product_value: product_value,
                            collect_value: collectable_value
                        })
                    }
                }

            } else if (restOfIndiaZoneData.length < 2 || restOfIndiaZoneData == '') {
                pincode_zones_data = []
            }
        }
    }
    return pincode_zones_data
}

exports.addressCheck = async (addressLength, customer_type) =>{
    console.log("addressCheck done")
    const addressData = await recommendation_config.find({
        address_length: {"$lt": addressLength},
        customer_type: customer_type
    })
    return addressData
}

exports.addressCheck2 = async (addressLength, customer_type) =>{
    console.log("addressCheck2 done")
    const addressData = await recommendation_config.find({
        address_length: {"$gt": addressLength},
        customer_type: customer_type
    })
    return addressData
}

exports.pincodePerform = async (addressLength, total_delivered_per, zone, customer_type) => {
    console.log("addressLength---total_delivered_per--zone--",addressLength, total_delivered_per, zone)
    const customerProfile = [];
    if(customerProfile.length == 0){
        const pincodeData = await recommendation_config.find({
            zone: zone,
            address_length: { "$lt": addressLength },
            pincode_greater: { "$lte": total_delivered_per },
            customer_type: customer_type,
            delivery_possibility: 'HIGH',
        })
        // console.log("@@@@---",pincodeData)
        if (pincodeData.length != 0) {
            for (let data of pincodeData) {
                customerProfile.push({
                    ruleId: data._id,
                    delivery_possibility: data.delivery_possibility,
                    flag: data.flag
                })
            }
        } else {
            customerProfile == []
        }
    }
    console.log("customerProfile---",customerProfile.length)
    if(customerProfile.length == 0){
        const pincodeData = await recommendation_config.find({
            zone: zone,
            address_length: { "$lt": addressLength },
            pincode_greater: { "$lte": total_delivered_per },
            pincode_less: { "$gt": total_delivered_per },
            customer_type:customer_type,
            delivery_possibility: 'Medium',
        })
        if (pincodeData.length != 0) {
            for (let data of pincodeData) {
                customerProfile.push({
                    ruleId: data._id,
                    delivery_possibility: data.delivery_possibility,
                    flag: data.flag
                })
            }
        } else {
            customerProfile == []
        }
    }
    if (customerProfile.length == 0) {
        const pincodeData = await recommendation_config.find({
            zone: zone,
            address_length: { "$lt": addressLength },
            pincode_less: { "$gt": total_delivered_per },
            customer_type: customer_type,
            delivery_possibility: 'Low',
        })
        if (pincodeData.length != 0) {
            for (let data of pincodeData) {
                customerProfile.push({
                    ruleId: data._id,
                    delivery_possibility: data.delivery_possibility,
                    flag: data.flag
                })
            }
        } else {
            customerProfile == []
        }
    }

    // if (customerProfile.length == 0) {
    //     const pincodeData = await recommendation_config.find({
    //         zone: zone,
    //         address_length: { "$gt": addressLength },
    //         customer_type: customer_type,
    //         delivery_possibility: 'Low',
    //     })
    //     if (pincodeData.length != 0) {
    //         for (let data of pincodeData) {
    //             customerProfile.push({
    //                 ruleId: data._id,
    //                 delivery_possibility: data.delivery_possibility,
    //                 flag: data.flag
    //             })
    //         }
    //     } else {
    //         customerProfile == []
    //     }
    // }
    // console.log("customerProfile1---",customerProfile)
    return customerProfile
}

exports.lessAddress = async (addressLength, zone, customer_type) => {
    console.log("lessaddressinput---",addressLength, zone, customer_type)
    const customerProfile = [];
    const newData = await recommendation_config.find({
        zone:zone,
        address_length: {"$gt": addressLength},
        customer_type: customer_type,
        flag: 'Short address',
        delivery_possibility: 'Low',
    })
    if(newData.length != 0){
        for(let data of newData){
            customerProfile.push({
                ruleId: data._id,
                delivery_possibility: data.delivery_possibility,
                flag: data.flag
            })
        }
    }else{
        customerProfile == []
    }
    return customerProfile
}

exports.getCustumProfile = async (address, zone, customer_type, mobileno, pay_type, pincode) => {
    const customerProfile = []
    var addressIs = '';
    var order_status = '';
    // console.log("address, zone---",address, zone)
    const addressData = await customer_performance_model.find({
        mobileno: mobileno,
        address: address,
        zone: zone
    })
    console.log("addressData---", addressData.length)
    if (addressData.length != '') {
        for await (let element of addressData) {
            if (element.total_delivered == 1 && element.total_shipments == 1) {
                console.log("==1 Delivered----same address")
                addressIs = 'If Same address';
                order_status = 'Delivered';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    order_status: order_status,
                    pay_type: pay_type
                })
                console.log("newData---", newData.length)
                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_delivered == 0 && element.total_shipments == 1) {
                console.log("==1 RTO----same address")
                addressIs = 'Not Applicable';
                order_status = 'RTO';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    order_status: order_status,
                    pay_type: pay_type
                })
                console.log("newData---", newData.length)
                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_shipments > 1) {

                console.log("same address  total_shipments > 1", element.total_delivered_per)
                addressIs = 'If Same address';
                var newData = []
                newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    status_greater: { '$lte': element.total_delivered_per },
                    customer_type: customer_type,
                    order_status: '',
                    pay_type: pay_type,
                    delivery_possibility: 'HIGH'
                })

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { '$lte': element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'HIGH'
                    })
                }
                console.log("newData---", newData.length)

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            }
        }
    } else {
        const addressData = await customer_performance_model.find({
            mobileno: mobileno,
            zone: zone,
            pincode: pincode
        })
        for await (let element of addressData) {
            if (element.total_delivered == 1 && element.total_shipments == 1) {
                console.log("different address--- delivered")
                addressIs = 'If different address';
                order_status = 'Delivered';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    pay_type: pay_type,
                    order_status: order_status
                })

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_delivered == 0 && element.total_shipments == 1) {
                console.log("different address --- RTO")
                addressIs = 'Not Applicable';
                order_status = 'RTO';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    pay_type: pay_type,
                    order_status: order_status
                })

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_shipments > 1) {
                console.log("different address --- total_shipments > 1")
                addressIs = 'If different address';
                var newData = []
                newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    status_greater: { '$lte': element.total_delivered_per },
                    customer_type: customer_type,
                    order_status: '',
                    pay_type: pay_type,
                    delivery_possibility: 'HIGH'
                })
                console.log("newData---", newData.length)
                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { '$lte': element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'HIGH'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            }
        }



    }
    return customerProfile
}

exports.getCustumProfile2 = async (address, zone, customer_type, mobileno, pay_type, pincode) => {
    const customerProfile = []
    var addressIs = '';
    var order_status = '';
    // console.log("address, zone---",address, zone)
    const addressData = await customer_performance_date_model.find({
        mobileno: mobileno,
        address: address,
        zone: zone
    })
    console.log("addressData---", addressData.length)
    if (addressData.length != '') {
        for await (let element of addressData) {
            if (element.total_delivered == 1 && element.total_shipments == 1) {
                console.log("==1 Delivered----same address")
                addressIs = 'If Same address';
                order_status = 'Delivered';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    order_status: order_status,
                    pay_type: pay_type
                })
                console.log("newData---", newData.length)
                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_delivered == 0 && element.total_shipments == 1) {
                console.log("==1 RTO----same address")
                addressIs = 'Not Applicable';
                order_status = 'RTO';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    order_status: order_status,
                    pay_type: pay_type
                })
                console.log("newData---", newData.length)
                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_shipments > 1) {

                console.log("same address  total_shipments > 1", element.total_delivered_per)
                addressIs = 'If Same address';
                var newData = []
                newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    status_greater: { '$lte': element.total_delivered_per },
                    customer_type: customer_type,
                    order_status: '',
                    pay_type: pay_type,
                    delivery_possibility: 'HIGH'
                })

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { '$lte': element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'HIGH'
                    })
                }
                console.log("newData---", newData.length)

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            }
        }
    } else {
        const addressData = await customer_performance_date_model.find({
            mobileno: mobileno,
            zone: zone,
            pincode: pincode
        })
        for await (let element of addressData) {
            if (element.total_delivered == 1 && element.total_shipments == 1) {
                console.log("different address--- delivered")
                addressIs = 'If different address';
                order_status = 'Delivered';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    pay_type: pay_type,
                    order_status: order_status
                })

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_delivered == 0 && element.total_shipments == 1) {
                console.log("different address --- RTO")
                addressIs = 'Not Applicable';
                order_status = 'RTO';
                const newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    customer_type: customer_type,
                    pay_type: pay_type,
                    order_status: order_status
                })

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            } else if (element.total_shipments > 1) {
                console.log("different address --- total_shipments > 1")
                addressIs = 'If different address';
                var newData = []
                newData = await recommendation_config.find({
                    zone: zone,
                    address: addressIs,
                    status_greater: { '$lte': element.total_delivered_per },
                    customer_type: customer_type,
                    order_status: '',
                    pay_type: pay_type,
                    delivery_possibility: 'HIGH'
                })
                console.log("newData---", newData.length)
                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { '$lte': element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'HIGH'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_greater: { "$lte": element.total_delivered_per },
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Medium'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: pay_type,
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length == 0) {
                    newData = await recommendation_config.find({
                        zone: zone,
                        address: addressIs,
                        status_less: { "$gt": element.total_delivered_per },
                        customer_type: customer_type,
                        order_status: '',
                        pay_type: 'both',
                        delivery_possibility: 'Low'
                    })
                }

                if (newData.length != 0) {
                    for (let data of newData) {
                        customerProfile.push({
                            ruleId: data._id,
                            delivery_possibility: data.delivery_possibility,
                            flag: data.flag
                        })
                    }
                } else {
                    customerProfile == []
                }
            }
        }



    }
    return customerProfile
}

exports.getRepeatProfile = async (address, zone, customer_type, mobileno, pay_type, pincode, myArray) => {
    console.log("############$$$$$$$$$---",address, zone, customer_type, mobileno, pay_type, pincode, myArray)
    var customerProfile = [];
    var itemArray = [];
    var addressIs = '';
    var order_status = '';
    var item  = myArray.find(item => item.mobileno === mobileno && item.zone === zone && item.address === address); 
          
    if(item){
      itemArray.push(item)
    }else{
      itemArray = itemArray;
    }
    // console.log("itemArray----",itemArray)
    if (itemArray.length != '') {
      for await (let element of itemArray) {
        if (element.totalDelivered == 1 && element.total_shipments == 1) {
          console.log("==1 Delivered----same address")
          addressIs = 'If Same address';
          order_status = 'Delivered';
          const newData = await recommendation_config.find({
              zone: zone,
              address: addressIs,
              customer_type: customer_type,
              order_status: order_status,
              pay_type: pay_type
          })
          console.log("newData---", newData.length)
          if (newData.length != 0) {
              for (let data of newData) {
                  customerProfile.push({
                      ruleId: data._id,
                      delivery_possibility: data.delivery_possibility,
                      flag: data.flag
                  })
              }
          } else {
              customerProfile == []
          }
      } else if (element.totalDelivered == 0 && element.total_shipments == 1) {
          console.log("==1 RTO----same address")
          addressIs = 'Not Applicable';
          order_status = 'RTO';
          const newData = await recommendation_config.find({
              zone: zone,
              address: addressIs,
              customer_type: customer_type,
              order_status: order_status,
              pay_type: pay_type
          })
          console.log("newData---", newData.length)
          if (newData.length != 0) {
              for (let data of newData) {
                  customerProfile.push({
                      ruleId: data._id,
                      delivery_possibility: data.delivery_possibility,
                      flag: data.flag
                  })
              }
          } else {
              customerProfile == []
          }
      } else if (element.total_shipments > 1) {

          console.log("same address  total_shipments > 1", element.total_delivered_per)
          addressIs = 'If Same address';
          var newData = []
          newData = await recommendation_config.find({
              zone: zone,
              address: addressIs,
              status_greater: { '$lte': element.total_delivered_per },
              customer_type: customer_type,
              order_status: '',
              pay_type: pay_type,
              delivery_possibility: 'HIGH'
          })

          if (newData.length == 0) {
              newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  status_greater: { '$lte': element.total_delivered_per },
                  customer_type: customer_type,
                  order_status: '',
                  pay_type: 'both',
                  delivery_possibility: 'HIGH'
              })
          }
          console.log("newData---", newData.length)

          if (newData.length == 0) {
              newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  status_greater: { "$lte": element.total_delivered_per },
                  status_less: { "$gt": element.total_delivered_per },
                  customer_type: customer_type,
                  order_status: '',
                  pay_type: pay_type,
                  delivery_possibility: 'Medium'
              })
          }

          if (newData.length == 0) {
              newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  status_greater: { "$lte": element.total_delivered_per },
                  status_less: { "$gt": element.total_delivered_per },
                  customer_type: customer_type,
                  order_status: '',
                  pay_type: 'both',
                  delivery_possibility: 'Medium'
              })
          }

          if (newData.length == 0) {
              newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  status_less: { "$gt": element.total_delivered_per },
                  customer_type: customer_type,
                  order_status: '',
                  pay_type: pay_type,
                  delivery_possibility: 'Low'
              })
          }

          if (newData.length == 0) {
              newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  status_less: { "$gt": element.total_delivered_per },
                  customer_type: customer_type,
                  order_status: '',
                  pay_type: 'both',
                  delivery_possibility: 'Low'
              })
          }

          if (newData.length != 0) {
              for (let data of newData) {
                  customerProfile.push({
                      ruleId: data._id,
                      delivery_possibility: data.delivery_possibility,
                      flag: data.flag
                  })
              }
          } else {
              customerProfile == []
          }
      }
      }
    }else{
      var item  = myArray.find(item => item.mobileno === mobileno && item.zone === zone && item.delivery_pincode === pincode); 
      if(item){
        itemArray.push(item)
      }else{
        itemArray = itemArray;
      }
    //   console.log("itemArray----",itemArray)
      if(itemArray.length != ''){
        for await (let element of itemArray) {
          if (element.totalDelivered == 1 && element.total_shipments == 1) {
              console.log("different address--- delivered")
              addressIs = 'If different address';
              order_status = 'Delivered';
              const newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  customer_type: customer_type,
                  pay_type: pay_type,
                  order_status: order_status
              })

              if (newData.length != 0) {
                  for (let data of newData) {
                      customerProfile.push({
                          ruleId: data._id,
                          delivery_possibility: data.delivery_possibility,
                          flag: data.flag
                      })
                  }
              } else {
                  customerProfile == []
              }
          } else if (element.totalDelivered == 0 && element.total_shipments == 1) {
              console.log("different address --- RTO")
              addressIs = 'Not Applicable';
              order_status = 'RTO';
              const newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  customer_type: customer_type,
                  pay_type: pay_type,
                  order_status: order_status
              })

              if (newData.length != 0) {
                  for (let data of newData) {
                      customerProfile.push({
                          ruleId: data._id,
                          delivery_possibility: data.delivery_possibility,
                          flag: data.flag
                      })
                  }
              } else {
                  customerProfile == []
              }
          } else if (element.total_shipments > 1) {
              console.log("different address --- total_shipments > 1")
              addressIs = 'If different address';
              var newData = []
              newData = await recommendation_config.find({
                  zone: zone,
                  address: addressIs,
                  status_greater: { '$lte': element.total_delivered_per },
                  customer_type: customer_type,
                  order_status: '',
                  pay_type: pay_type,
                  delivery_possibility: 'HIGH'
              })
              console.log("newData---", newData.length)
              if (newData.length == 0) {
                  newData = await recommendation_config.find({
                      zone: zone,
                      address: addressIs,
                      status_greater: { '$lte': element.total_delivered_per },
                      customer_type: customer_type,
                      order_status: '',
                      pay_type: 'both',
                      delivery_possibility: 'HIGH'
                  })
              }

              if (newData.length == 0) {
                  newData = await recommendation_config.find({
                      zone: zone,
                      address: addressIs,
                      status_greater: { "$lte": element.total_delivered_per },
                      status_less: { "$gt": element.total_delivered_per },
                      customer_type: customer_type,
                      order_status: '',
                      pay_type: pay_type,
                      delivery_possibility: 'Medium'
                  })
              }

              if (newData.length == 0) {
                  newData = await recommendation_config.find({
                      zone: zone,
                      address: addressIs,
                      status_greater: { "$lte": element.total_delivered_per },
                      status_less: { "$gt": element.total_delivered_per },
                      customer_type: customer_type,
                      order_status: '',
                      pay_type: 'both',
                      delivery_possibility: 'Medium'
                  })
              }

              if (newData.length == 0) {
                  newData = await recommendation_config.find({
                      zone: zone,
                      address: addressIs,
                      status_less: { "$gt": element.total_delivered_per },
                      customer_type: customer_type,
                      order_status: '',
                      pay_type: pay_type,
                      delivery_possibility: 'Low'
                  })
              }

              if (newData.length == 0) {
                  newData = await recommendation_config.find({
                      zone: zone,
                      address: addressIs,
                      status_less: { "$gt": element.total_delivered_per },
                      customer_type: customer_type,
                      order_status: '',
                      pay_type: 'both',
                      delivery_possibility: 'Low'
                  })
              }

              if (newData.length != 0) {
                  for (let data of newData) {
                      customerProfile.push({
                          ruleId: data._id,
                          delivery_possibility: data.delivery_possibility,
                          flag: data.flag
                      })
                  }
              } else {
                  customerProfile == []
              }
          }
      }
      }else{
        customerProfile = customerProfile
      }
    }
    return customerProfile
}

exports.getPincodeZoneData = async (pincode, zone, myArray) => {
    var itemArray = [];
    var item  = myArray.find(item => item.delivery_pincode === pincode && item.zone === zone); 
          
    if(item){
      itemArray.push(item)
    }else{
      itemArray = itemArray;
    }
    return itemArray
}