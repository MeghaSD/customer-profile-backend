const deliveryPossibilityModel = require('../models/delivery_possibility_masters');
var trim = require('trim');

exports.saveDeliveryPossibility = async (req, res, next) => {
    if (req.body.delivery_possibility_label && (req.body.delivery_possibility_label !== undefined || req.body.delivery_possibility_label !== 'undefined' || req.body.delivery_possibility_label !== '') && req.body.delivery_possibility_value && (req.body.delivery_possibility_value !== undefined || req.body.delivery_possibility_value !== 'undefined' || req.body.delivery_possibility_value !== '')) {
        var delivery_possibility_label = trim(req.body.delivery_possibility_label)
        var delivery_possibility_value = trim(req.body.delivery_possibility_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await deliveryPossibilityModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    deliveryPossibilityModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                delivery_possibility_label: delivery_possibility_label,
                                delivery_possibility_value: delivery_possibility_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Delivery Status Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Delivery Status Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var deliveryStatus = new deliveryPossibilityModel({
                delivery_possibility_label: delivery_possibility_label,
                delivery_possibility_value: delivery_possibility_value
            });
            deliveryStatus.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Delivery Status Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Delivery Status Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getDeliveryMasters = async (req, res, next) => {
    try {
        const deliveryPossibility = await deliveryPossibilityModel.find({});
        if (deliveryPossibility != '') {
            res.json({
                status: 200,
                message: 'success',
                data: deliveryPossibility,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: deliveryPossibility
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}