const flagModel = require('../models/flag_masters');
var trim = require('trim');

exports.saveFlag = async (req, res, next) => {
    if (req.body.flag_label && (req.body.flag_label !== undefined || req.body.flag_label !== 'undefined' || req.body.flag_label !== '') && req.body.flag_value && (req.body.flag_value !== undefined || req.body.flag_value !== 'undefined' || req.body.flag_value !== '')) {
        var flag_label = trim(req.body.flag_label)
        var flag_value = trim(req.body.flag_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await flagModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    flagModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                flag_label: flag_label,
                                flag_value: flag_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Flag Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Flag Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var flagData = new flagModel({
                flag_label: flag_label,
                flag_value: flag_value
            });
            flagData.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Flag Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Flag Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getFlagMasters = async (req, res, next) => {
    try {
        const flags = await flagModel.find({});
        if (flags != '') {
            res.json({
                status: 200,
                message: 'success',
                data: flags,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: flags
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}