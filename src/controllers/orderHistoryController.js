const orderHistoryModel = require('../models/order_history_masters');
var trim = require('trim');

exports.saveOrderHistory = async (req, res, next) => {
    if (req.body.order_history_label && (req.body.order_history_label !== undefined || req.body.order_history_label !== 'undefined' || req.body.order_history_label !== '') && req.body.order_history_value && (req.body.order_history_value !== undefined || req.body.order_history_value !== 'undefined' || req.body.order_history_value !== '')) {
        var order_history_label = trim(req.body.order_history_label)
        var order_history_value = trim(req.body.order_history_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await orderHistoryModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    orderHistoryModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                order_history_label: order_history_label,
                                order_history_value: order_history_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Order History Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Order History Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var orderHistory = new orderHistoryModel({
                order_history_label: order_history_label,
                order_history_value: order_history_value
            });
            orderHistory.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Order History Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Order History Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getOrdersHistoryMasters = async (req, res, next) => {
    try {
        if(req.query.type &&(req.query.type != undefined || req.query.type != 'undefined' || req.query.type != null)){
            var type = trim(req.query.type);
            var orderHistory = [];
            if(type == 'New'){
                orderHistory = await orderHistoryModel.find({
                    order_history_label: 'Not Applicable'
                });
            }else{
                orderHistory = await orderHistoryModel.find({
                    order_history_label: {
                        '$ne': 'Not Applicable'
                    }
                });
            }
            
        if (orderHistory != '') {
            res.json({
                status: 200,
                message: 'success',
                data: orderHistory,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: orderHistory
            })
        }
        }else{
            res.json({
                status:401,
                message: 'Field is required'
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}
