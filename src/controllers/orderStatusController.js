const orderStatusModel = require('../models/order_status_masters');
var trim = require('trim');

exports.saveOrderStatus = async (req, res, next) => {
    if (req.body.order_status_label && (req.body.order_status_label !== undefined || req.body.order_status_label !== 'undefined' || req.body.order_status_label !== '') && req.body.order_status_value && (req.body.order_status_value !== undefined || req.body.order_status_value !== 'undefined' || req.body.order_status_value !== '')) {
        var order_status_label = trim(req.body.order_status_label)
        var order_status_value = trim(req.body.order_status_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await orderStatusModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    orderStatusModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                order_status_label: order_status_label,
                                order_status_value: order_status_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Order Status Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Order Status Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var orderStatus = new orderStatusModel({
                order_status_label: order_status_label,
                order_status_value: order_status_value
            });
            orderStatus.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Order Status Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Order Status Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getOrdersStatusMasters = async (req, res, next) => {
    try {
        if(req.query.type &&(req.query.type != undefined || req.query.type != 'undefined' || req.query.type != null)){
            var type = trim(req.query.type);
            var orderStatus = [];
            if(type == 'New'){
                orderStatus = await orderStatusModel.find({
                    order_status_label: 'Not Applicable'
                });
            }else{
                orderStatus = await orderStatusModel.find({
                    order_status_label: {
                        '$ne':'Not Applicable'}
                });
            }
            if (orderStatus != '') {
                res.json({
                    status: 200,
                    message: 'success',
                    data: orderStatus,
                })
            } else {
                res.json({
                    status: 400,
                    message: 'Not Data Found',
                    data: orderStatus
                })
            }
        }else{
            res.json({
                status:401,
                message: 'Field is required'
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}