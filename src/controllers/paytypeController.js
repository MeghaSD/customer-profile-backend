const payTypeModel = require('../models/paytype_masters');
var trim = require('trim');

exports.savePayType = async (req, res, next) => {
    if (req.body.pay_type_label && (req.body.pay_type_label !== undefined || req.body.pay_type_label !== 'undefined' || req.body.pay_type_label !== '') && req.body.pay_type_value && (req.body.pay_type_value !== undefined || req.body.pay_type_value !== 'undefined' || req.body.pay_type_value !== '')) {
        var pay_type_label = trim(req.body.pay_type_label)
        var pay_type_value = trim(req.body.pay_type_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await payTypeModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    payTypeModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                pay_type_label: pay_type_label,
                                pay_type_value: pay_type_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Pay_type Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Pay_type Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var payTypeData = new payTypeModel({
                pay_type_label: pay_type_label,
                pay_type_value: pay_type_value
            });
            payTypeData.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Pay_type Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Pay_type Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getPaytypeMasters = async (req, res, next) => {
    try {
        if(req.query.type &&(req.query.type != undefined || req.query.type != 'undefined' || req.query.type != null)){
            var type = trim(req.query.type);
            var paytype = [];
            if(type == 'New'){
                paytype = await payTypeModel.find({
                    pay_type_label: 'Not Applicable'
                });
            }else{
                paytype = await payTypeModel.find({
                    pay_type_label: {
                        '$ne': 'Not Applicable'
                    }
                });
            }
            
        if (paytype != '') {
            res.json({
                status: 200,
                message: 'success',
                data: paytype,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: paytype
            })
        }
        }else{
            res.json({
                status:401,
                message: 'Field is required'
            })
        }
        
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}