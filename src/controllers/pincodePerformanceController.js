const pincodePerformanceModel = require('../models/pincode_performance_masters');
var trim = require('trim');

exports.savePincodePerformance = async (req, res, next) => {
    if (req.body.pincode_label && (req.body.pincode_label !== undefined || req.body.pincode_label !== 'undefined' || req.body.pincode_label !== '') && req.body.pincode_value && (req.body.pincode_value !== undefined || req.body.pincode_value !== 'undefined' || req.body.pincode_value !== '')) {
        var pincode_label = trim(req.body.pincode_label)
        var pincode_value = trim(req.body.pincode_value)
        var existsData = []
        if (req.body._id) {
            var _id = trim(req.body._id)
            existsData = await pincodePerformanceModel.find({
                _id: _id
            })
            if (existsData.length != 0) {
                try {
                    pincodePerformanceModel.findOneAndUpdate(
                        {
                            _id: _id,
                        },
                        {
                            $set: {
                                pincode_label: pincode_label,
                                pincode_value: pincode_value
                            }
                        },
                        { upsert: true, new: true },
                        function (err, data) {
                            if (err) {
                                res.json({
                                    status: 400,
                                    message: 'Some Error Occured During Pincode Performance Creation.'
                                });
                            } else {
                                res.status(200).json({
                                    message: "Pincode Performance Save Successfully",
                                });
                            }
                        });
                } catch (error) {
                    return res.status(500).json({
                        error: 'Internal Server Error'
                    })
                }
            }
        } else {
            var pincodePerformance = new pincodePerformanceModel({
                pincode_label: pincode_label,
                pincode_value: pincode_value
            });
            pincodePerformance.save(function (err, result) {
                if (err) {
                    res.json({
                        status: 400,
                        message: 'Some Error Occured During Pincode Performance Creation.'
                    })
                } else {
                    res.json({
                        status: 200,
                        message: 'Pincode Performance Save Successfully'
                    });
                }
            });

        }

    } else {
        res.json({
            status: 400,
            message: 'All fields are required..'
        })
    }

};

exports.getPincodeMasters = async (req, res, next) => {
    try {
        const pincodePerformance = await pincodePerformanceModel.find({});
        if (pincodePerformance != '') {
            res.json({
                status: 200,
                message: 'success',
                data: pincodePerformance,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: pincodePerformance
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}