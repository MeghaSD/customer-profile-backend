const pickupDao = require("../dao/pickup_dao");
const ilogix_standard_pincodes_master = require("../dao/ilogix_standard_pincode_dao");
const commanController = require("../controllers/commanController");
const pincode_zones = require("../dao/pincode_zones_dao");
const { profile } = require("winston");
const pincode_performance_model = require("../models/pincode_performance");
const customer_performance_model = require("../models/customer_performance");
const pincode_performance_date_model = require("../models/pincode_performance_date");
const customer_performance_date_model = require("../models/customer_performance_date");
const recommendation_config = require("../models/recommendation");
const trim = require("trim");

exports.savePincodeZones = async (req, res, next) => {
  try {
    pickupDao.getPickups().then(async data => {
      if (data) {
        for (let element of data) {
          commanController.getZonesData(element.sub_vendor_pincode, element.pincode, element.airwaybilno, element.id, element.telephone1, element.dispatch_status, element.product, element.consignee_email, element.consignee_address1, element.product_value, element.collectable_value).then(async function (pincode_zones_data) {
            // console.log("getzones---",pincode_zones_data)
            if (pincode_zones_data != '' && pincode_zones_data != undefined) {
              for await (let pincode of pincode_zones_data) {
                pincode_zones.saveData(pincode.airwaybilno, pincode.zone, pincode.pickup_id, pincode.mobileno, pincode.dispatch_status, pincode.pay_type, pincode.delivery_pincode, pincode.email_id, pincode.address, pincode.product_value, pincode.collect_value).then(async function (save) {
                  if (save) {
                    console.log("save successfully")
                  } else {
                    console.log("not save")
                  }

                })
              }
            }
          })
        }
        await pincode_zones.updatePaytypeCOD().then(function (updateCOD) {
          if (updateCOD) {
            console.log("update pay_type COD")
          } else {
            console.log("no data ")
          }
        })
        await pincode_zones.updatePaytypePPD().then(function (updatePPD) {
          if (updatePPD) {
            console.log("update pay_type PPD")
          } else {
            console.log("no data ")
          }
        })
        console.log("datalength--", data.length)
      } else {
        console.log("No Data in pickup table")
      }
      res.send(data)
    })
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
};

exports.savePincodePerformanceData = async (req, res, next) => {
  try {
    var newArray = [];
    var profileData = await pickupDao.pincodePerformance()
    // console.log("profileData---",profileData)
    if (profileData) {
      for await (let data of profileData) {
        const found = newArray.some(el => el.delivery_pincode === data.delivery_pincode && el.zone === data.zone && el.dispatch_status === data.dispatch_status);
        var totalCOD = 0;
        var totalPPD = 0;
        if (!found) {
          if (data.pay_type == 'COD') {
            await newArray.push({
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              dispatch_status: data.dispatch_status,
              totalCOD: data.total,
              totalPPD: totalPPD,
              total_shipments: data.total
            });
          } else if (data.pay_type == 'PPD') {
            await newArray.push({
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              dispatch_status: data.dispatch_status,
              totalCOD: totalCOD,
              totalPPD: data.total,
              total_shipments: data.total
            });
          }

        } else {
          if (data.pay_type == 'COD') {
            var objIndex = newArray.findIndex((obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status));
            newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD + data.total,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD
            newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total

          } else if (data.pay_type == 'PPD') {
            var objIndex = newArray.findIndex(obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status);
            newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD + data.total,
              newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total
          }

        }
      }
      var myArray = [];
      for await (let element of newArray) {
        const found1 = myArray.some(el => el.delivery_pincode === element.delivery_pincode && el.zone === element.zone);
        if (!found1) {
          if (element.dispatch_status == '5') {
            if (element.total_shipments != 0) {
              var totalDelivered = element.total_shipments
              // console.log("@@@1",element.delivery_pincode,element.zone, element.total_shipments , element.total_shipments)
              var totalDeliveredPer = (element.total_shipments / element.total_shipments) * 100
              var totalCODDelivered = element.totalCOD
              var totalPPDDelivered = element.totalPPD
              if (element.totalCOD != 0) {
                var totalCODDeliveredPer = (element.totalCOD / element.total_shipments) * 100
              } else {
                var totalCODDeliveredPer = 0
              }

              if (element.totalPPD != 0) {
                var totalPPDDeliveredPer = (element.totalPPD / element.total_shipments) * 100
              } else {
                var totalPPDDeliveredPer = 0
              }
              // console.log("@@@1", totalCODDeliveredPer)
            } else {
              var totalDelivered = 0
              var totalDeliveredPer = 0
              var totalCODDelivered = 0
              var totalPPDDelivered = 0
              var totalCODDeliveredPer = 0
              var totalPPDDeliveredPer = 0
            }

          } else {

            var totalDelivered = 0
            var totalDeliveredPer = 0
            var totalCODDelivered = 0
            var totalPPDDelivered = 0
            var totalCODDeliveredPer = 0
            var totalPPDDeliveredPer = 0

          }
          await myArray.push({
            delivery_pincode: element.delivery_pincode,
            zone: element.zone,
            totalCOD: element.totalCOD,
            totalPPD: element.totalPPD,
            totalDelivered: totalDelivered,
            totalDeliveredPer: totalDeliveredPer,
            totalCODDelivered: totalCODDelivered,
            totalPPDDelivered: totalPPDDelivered,
            totalCODDeliveredPer: totalCODDeliveredPer,
            totalPPDDeliveredPer: totalPPDDeliveredPer,
            total_shipments: element.total_shipments
          });
        } else {
          var objIndex = myArray.findIndex(obj => obj.delivery_pincode == element.delivery_pincode && obj.zone == element.zone);
            myArray[objIndex].delivery_pincode = myArray[objIndex].delivery_pincode,
            myArray[objIndex].zone = myArray[objIndex].zone,
            myArray[objIndex].totalCOD =  myArray[objIndex].totalCOD + element.totalCOD,
            myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
            myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
          if (element.dispatch_status == '5') {
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = (myArray[objIndex].totalDelivered + element.total_shipments)
              // console.log("@@@2",element.delivery_pincode,element.zone, myArray[objIndex].totalDelivered , myArray[objIndex].total_shipments)

              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered + element.totalCOD
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered + element.totalPPD

              if (myArray[objIndex].totalCODDelivered != 0) {
                // console.log("totalCODDelivered1---", element.delivery_pincode ,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
                myArray[objIndex].totalCODDeliveredPer = (myArray[objIndex].totalCODDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalCODDeliveredPer = 0
                // console.log("totalCODDelivered2---" ,element.delivery_pincode,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)

              }

              if (myArray[objIndex].totalPPDDelivered != 0) {
                myArray[objIndex].totalPPDDeliveredPer = (myArray[objIndex].totalPPDDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalPPDDeliveredPer = 0
              }
              
            } else {
              myArray[objIndex].totalDelivered = 0
              myArray[objIndex].totalDeliveredPer = 0
              myArray[objIndex].totalCODDelivered = 0
              myArray[objIndex].totalPPDDelivered = 0
              myArray[objIndex].totalCODDeliveredPer = 0
              myArray[objIndex].totalPPDDeliveredPer = 0
            }

          } else {
            myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
            myArray[objIndex].totalCOD =  myArray[objIndex].totalCOD + element.totalCOD,
            myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered 
              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered 
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            } else {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
              myArray[objIndex].totalDeliveredPer =  myArray[objIndex].totalDeliveredPer
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered 
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            }


            // if (myArray[objIndex].total_shipments != 0) {
            

          }
        }

      }
      for await(let pincode of myArray){
        pincode_performance_model.findOneAndUpdate(
          {
            pincode: trim(pincode.delivery_pincode),
            zone: trim(pincode.zone)
          },
          {
            $set: {
              total_shipments: pincode.total_shipments,
              total_delivered: pincode.totalDelivered,
              total_delivered_per: Math.round(pincode.totalDeliveredPer),
              total_cod_shipments: pincode.totalCOD,
              total_cod_delivered: pincode.totalCODDelivered,
              total_cod_delivered_per: Math.round(pincode.totalCODDeliveredPer),
              total_ppd_shipments: pincode.totalPPD,
              total_ppd_delivered: pincode.totalPPDDelivered,
              total_ppd_delivered_per: Math.round(pincode.totalPPDDeliveredPer),
            }
        },
        { upsert: true, new: true },
        function (err, pincode_performance) {
            if (err) {
               console.log("err---",err)
            } else {
                // console.log("pincode performance save successfully")
            }
        });
      }
      res.json({
        status: 200,
        data: myArray
      })
    } else {
      res.json({
        status: 400,
        data: myArray
      })
    }

  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
}

exports.saveCronWisePincodePerformanceData = async (tableName) => {
  
    var newArray = [];
    var profileData = await pickupDao.pincodePerformance1(tableName)
    // console.log("profileData---",profileData)
    if (profileData) {
      for await (let data of profileData) {
        const found = newArray.some(el => el.delivery_pincode === data.delivery_pincode && el.zone === data.zone && el.dispatch_status === data.dispatch_status);
        var totalCOD = 0;
        var totalPPD = 0;
        if (!found) {
          if (data.pay_type == 'COD') {
            await newArray.push({
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              dispatch_status: data.dispatch_status,
              totalCOD: data.total,
              totalPPD: totalPPD,
              total_shipments: data.total
            });
          } else if (data.pay_type == 'PPD') {
            await newArray.push({
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              dispatch_status: data.dispatch_status,
              totalCOD: totalCOD,
              totalPPD: data.total,
              total_shipments: data.total
            });
          }

        } else {
          if (data.pay_type == 'COD') {
            var objIndex = newArray.findIndex((obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status));
            newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD + data.total,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD
            newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total

          } else if (data.pay_type == 'PPD') {
            var objIndex = newArray.findIndex(obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status);
            newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD + data.total,
              newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total
          }

        }
      }
      var myArray = [];
      for await (let element of newArray) {
        const found1 = myArray.some(el => el.delivery_pincode === element.delivery_pincode && el.zone === element.zone);
        if (!found1) {
          if (element.dispatch_status == '5') {
            if (element.total_shipments != 0) {
              var totalDelivered = element.total_shipments
              // console.log("@@@1",element.delivery_pincode,element.zone, element.total_shipments , element.total_shipments)
              var totalDeliveredPer = (element.total_shipments / element.total_shipments) * 100
              var totalCODDelivered = element.totalCOD
              var totalPPDDelivered = element.totalPPD
              if (element.totalCOD != 0) {
                var totalCODDeliveredPer = (element.totalCOD / element.total_shipments) * 100
              } else {
                var totalCODDeliveredPer = 0
              }

              if (element.totalPPD != 0) {
                var totalPPDDeliveredPer = (element.totalPPD / element.total_shipments) * 100
              } else {
                var totalPPDDeliveredPer = 0
              }
              // console.log("@@@1", totalCODDeliveredPer)
            } else {
              var totalDelivered = 0
              var totalDeliveredPer = 0
              var totalCODDelivered = 0
              var totalPPDDelivered = 0
              var totalCODDeliveredPer = 0
              var totalPPDDeliveredPer = 0
            }

          } else {

            var totalDelivered = 0
            var totalDeliveredPer = 0
            var totalCODDelivered = 0
            var totalPPDDelivered = 0
            var totalCODDeliveredPer = 0
            var totalPPDDeliveredPer = 0

          }
          await myArray.push({
            delivery_pincode: element.delivery_pincode,
            zone: element.zone,
            totalCOD: element.totalCOD,
            totalPPD: element.totalPPD,
            totalDelivered: totalDelivered,
            totalDeliveredPer: totalDeliveredPer,
            totalCODDelivered: totalCODDelivered,
            totalPPDDelivered: totalPPDDelivered,
            totalCODDeliveredPer: totalCODDeliveredPer,
            totalPPDDeliveredPer: totalPPDDeliveredPer,
            total_shipments: element.total_shipments
          });
        } else {
          var objIndex = myArray.findIndex(obj => obj.delivery_pincode == element.delivery_pincode && obj.zone == element.zone);
            myArray[objIndex].delivery_pincode = myArray[objIndex].delivery_pincode,
            myArray[objIndex].zone = myArray[objIndex].zone,
            myArray[objIndex].totalCOD =  myArray[objIndex].totalCOD + element.totalCOD,
            myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
            myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
          if (element.dispatch_status == '5') {
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = (myArray[objIndex].totalDelivered + element.total_shipments)
              // console.log("@@@2",element.delivery_pincode,element.zone, myArray[objIndex].totalDelivered , myArray[objIndex].total_shipments)

              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered + element.totalCOD
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered + element.totalPPD

              if (myArray[objIndex].totalCODDelivered != 0) {
                // console.log("totalCODDelivered1---", element.delivery_pincode ,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
                myArray[objIndex].totalCODDeliveredPer = (myArray[objIndex].totalCODDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalCODDeliveredPer = 0
                // console.log("totalCODDelivered2---" ,element.delivery_pincode,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)

              }

              if (myArray[objIndex].totalPPDDelivered != 0) {
                myArray[objIndex].totalPPDDeliveredPer = (myArray[objIndex].totalPPDDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalPPDDeliveredPer = 0
              }
              
            } else {
              myArray[objIndex].totalDelivered = 0
              myArray[objIndex].totalDeliveredPer = 0
              myArray[objIndex].totalCODDelivered = 0
              myArray[objIndex].totalPPDDelivered = 0
              myArray[objIndex].totalCODDeliveredPer = 0
              myArray[objIndex].totalPPDDeliveredPer = 0
            }

          } else {
            myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
            myArray[objIndex].totalCOD =  myArray[objIndex].totalCOD + element.totalCOD,
            myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered 
              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered 
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            } else {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
              myArray[objIndex].totalDeliveredPer =  myArray[objIndex].totalDeliveredPer
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered 
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            }


            // if (myArray[objIndex].total_shipments != 0) {
            

          }
        }

      }
     console.log("success")
      
    } else {
     console.log("no profile data")
    }
    return myArray
}

exports.saveCustomerPerformance = async (req, res, next) => {
  try {
    var newArray = [];
    var customerData = await pickupDao.customerPerformance()
    // console.log("length---", customerData.length)
    if (customerData) {
      for await (let data of customerData) {
        const found = newArray.some(el => el.delivery_pincode === data.delivery_pincode && el.zone === data.zone && el.dispatch_status === data.dispatch_status && el.mobileno === data.mobileno && el.address === data.address);
        var totalCOD = 0;
        var totalPPD = 0;
        if (!found) {
          if (data.pay_type == 'COD') {
            await newArray.push({
              mobileno: data.mobileno,
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              address: data.address,
              dispatch_status: data.dispatch_status,
              totalCOD: data.total,
              totalPPD: totalPPD,
              total_shipments: data.total
            });
          } else if (data.pay_type == 'PPD') {
            await newArray.push({
              mobileno: data.mobileno,
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              address: data.address,
              dispatch_status: data.dispatch_status,
              totalCOD: totalCOD,
              totalPPD: data.total,
              total_shipments: data.total
            });
          }

        } else {
          if (data.pay_type == 'COD') {
            var objIndex = newArray.findIndex((obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status && obj.mobileno == data.mobileno && obj.address == data.address));
            newArray[objIndex].mobileno = data.mobileno,
              newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].address = data.address,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD + data.total,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD
            newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total

          } else if (data.pay_type == 'PPD') {
            var objIndex = newArray.findIndex(obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status && obj.mobileno == data.mobileno && obj.address == data.address);
            newArray[objIndex].mobileno = data.mobileno,
              newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].address = data.address,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD + data.total,
              newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total
          }

        }
      }
      var myArray = [];
      for await (let element of newArray) {
        const found1 = myArray.some(el => el.delivery_pincode === element.delivery_pincode && el.zone === element.zone && el.mobileno == element.mobileno && el.address == element.address);
        if (!found1) {
          if (element.dispatch_status == '5') {
            if (element.total_shipments != 0) {
              var totalDelivered = element.total_shipments
              // console.log("@@@1",element.delivery_pincode,element.zone, element.total_shipments , element.total_shipments)
              var totalDeliveredPer = (element.total_shipments / element.total_shipments) * 100
              var totalCODDelivered = element.totalCOD
              var totalPPDDelivered = element.totalPPD
              if (element.totalCOD != 0) {
                var totalCODDeliveredPer = (element.totalCOD / element.total_shipments) * 100
              } else {
                var totalCODDeliveredPer = 0
              }

              if (element.totalPPD != 0) {
                var totalPPDDeliveredPer = (element.totalPPD / element.total_shipments) * 100
              } else {
                var totalPPDDeliveredPer = 0
              }
              // console.log("@@@1", totalCODDeliveredPer)
            } else {
              var totalDelivered = 0
              var totalDeliveredPer = 0
              var totalCODDelivered = 0
              var totalPPDDelivered = 0
              var totalCODDeliveredPer = 0
              var totalPPDDeliveredPer = 0
            }

          } else {

            var totalDelivered = 0
            var totalDeliveredPer = 0
            var totalCODDelivered = 0
            var totalPPDDelivered = 0
            var totalCODDeliveredPer = 0
            var totalPPDDeliveredPer = 0

          }
          await myArray.push({
            mobileno: element.mobileno,
            delivery_pincode: element.delivery_pincode,
            zone: element.zone,
            address: element.address,
            totalCOD: element.totalCOD,
            totalPPD: element.totalPPD,
            totalDelivered: totalDelivered,
            totalDeliveredPer: totalDeliveredPer,
            totalCODDelivered: totalCODDelivered,
            totalPPDDelivered: totalPPDDelivered,
            totalCODDeliveredPer: totalCODDeliveredPer,
            totalPPDDeliveredPer: totalPPDDeliveredPer,
            total_shipments: element.total_shipments
          });
        } else {
          var objIndex = myArray.findIndex(obj => obj.delivery_pincode == element.delivery_pincode && obj.zone == element.zone && obj.mobileno == element.mobileno && obj.address == element.address);
          myArray[objIndex].mobileno = myArray[objIndex].mobileno,
            myArray[objIndex].delivery_pincode = myArray[objIndex].delivery_pincode,
            myArray[objIndex].zone = myArray[objIndex].zone,
            myArray[objIndex].address = myArray[objIndex].address,
            myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
            myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
          myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
          if (element.dispatch_status == '5') {
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = (myArray[objIndex].totalDelivered + element.total_shipments)
              // console.log("@@@2",element.delivery_pincode,element.zone, myArray[objIndex].totalDelivered , myArray[objIndex].total_shipments)

              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered + element.totalCOD
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered + element.totalPPD

              if (myArray[objIndex].totalCODDelivered != 0) {
                // console.log("totalCODDelivered1---", element.delivery_pincode ,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
                myArray[objIndex].totalCODDeliveredPer = (myArray[objIndex].totalCODDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalCODDeliveredPer = 0
                // console.log("totalCODDelivered2---" ,element.delivery_pincode,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)

              }

              if (myArray[objIndex].totalPPDDelivered != 0) {
                myArray[objIndex].totalPPDDeliveredPer = (myArray[objIndex].totalPPDDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalPPDDeliveredPer = 0
              }

            } else {
              myArray[objIndex].totalDelivered = 0
              myArray[objIndex].totalDeliveredPer = 0
              myArray[objIndex].totalCODDelivered = 0
              myArray[objIndex].totalPPDDelivered = 0
              myArray[objIndex].totalCODDeliveredPer = 0
              myArray[objIndex].totalPPDDeliveredPer = 0
            }

          } else {
            myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
            myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
              myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            } else {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
              myArray[objIndex].totalDeliveredPer = myArray[objIndex].totalDeliveredPer
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            }


            // if (myArray[objIndex].total_shipments != 0) {


          }
        }

      }
      for await(let pincode of myArray){
        customer_performance_model.findOneAndUpdate(
          {
            mobileno: trim(pincode.mobileno),
            pincode: trim(pincode.delivery_pincode),
            zone: trim(pincode.zone),
            address: trim(pincode.address)
          },
          {
            $set: {
              total_shipments: pincode.total_shipments,
              total_delivered: pincode.totalDelivered,
              total_delivered_per: Math.round(pincode.totalDeliveredPer),
              total_cod_shipments: pincode.totalCOD,
              total_cod_delivered: pincode.totalCODDelivered,
              total_cod_delivered_per: Math.round(pincode.totalCODDeliveredPer),
              total_ppd_shipments: pincode.totalPPD,
              total_ppd_delivered: pincode.totalPPDDelivered,
              total_ppd_delivered_per: Math.round(pincode.totalPPDDeliveredPer),
            }
        },
        { upsert: true, new: true },
        function (err, customer_performance) {
            if (err) {
               console.log("err---",err)
            } else {
                // console.log("customer performance save successfully")
            }
        });
      }
      res.json({
        status: 200,
        data: myArray
      })
    } else {
      res.json({
        status: 400,
        data: myArray
      })
    }
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
}

exports.saveCronWiseCustomerPerformance = async (tableName) => {
  console.log("tableName---",tableName)
  // try {
    var newArray = [];
    var myArray = [];
    var customerData = await pickupDao.customerPerformance1(tableName)
    // console.log("length---", customerData.length)
    if (customerData) {
      for await (let data of customerData) {
        const found = newArray.some(el => el.delivery_pincode === data.delivery_pincode && el.zone === data.zone && el.dispatch_status === data.dispatch_status && el.mobileno === data.mobileno && el.address === data.address);
        var totalCOD = 0;
        var totalPPD = 0;
        if (!found) {
          if (data.pay_type == 'COD') {
            await newArray.push({
              mobileno: data.mobileno,
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              address: data.address,
              dispatch_status: data.dispatch_status,
              totalCOD: data.total,
              totalPPD: totalPPD,
              total_shipments: data.total
            });
          } else if (data.pay_type == 'PPD') {
            await newArray.push({
              mobileno: data.mobileno,
              delivery_pincode: data.delivery_pincode,
              zone: data.zone,
              address: data.address,
              dispatch_status: data.dispatch_status,
              totalCOD: totalCOD,
              totalPPD: data.total,
              total_shipments: data.total
            });
          }

        } else {
          if (data.pay_type == 'COD') {
            var objIndex = newArray.findIndex((obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status && obj.mobileno == data.mobileno && obj.address == data.address));
            newArray[objIndex].mobileno = data.mobileno,
              newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].address = data.address,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD + data.total,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD
            newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total

          } else if (data.pay_type == 'PPD') {
            var objIndex = newArray.findIndex(obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status && obj.mobileno == data.mobileno && obj.address == data.address);
            newArray[objIndex].mobileno = data.mobileno,
              newArray[objIndex].delivery_pincode = data.delivery_pincode,
              newArray[objIndex].zone = data.zone,
              newArray[objIndex].address = data.address,
              newArray[objIndex].dispatch_status = data.dispatch_status,
              newArray[objIndex].totalCOD = newArray[objIndex].totalCOD,
              newArray[objIndex].totalPPD = newArray[objIndex].totalPPD + data.total,
              newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + data.total
          }

        }
      }
     
      for await (let element of newArray) {
        const found1 = myArray.some(el => el.delivery_pincode === element.delivery_pincode && el.zone === element.zone && el.mobileno == element.mobileno && el.address == element.address);
        if (!found1) {
          if (element.dispatch_status == '5') {
            if (element.total_shipments != 0) {
              var totalDelivered = element.total_shipments
              // console.log("@@@1",element.delivery_pincode,element.zone, element.total_shipments , element.total_shipments)
              var totalDeliveredPer = (element.total_shipments / element.total_shipments) * 100
              var totalCODDelivered = element.totalCOD
              var totalPPDDelivered = element.totalPPD
              if (element.totalCOD != 0) {
                var totalCODDeliveredPer = (element.totalCOD / element.total_shipments) * 100
              } else {
                var totalCODDeliveredPer = 0
              }

              if (element.totalPPD != 0) {
                var totalPPDDeliveredPer = (element.totalPPD / element.total_shipments) * 100
              } else {
                var totalPPDDeliveredPer = 0
              }
              // console.log("@@@1", totalCODDeliveredPer)
            } else {
              var totalDelivered = 0
              var totalDeliveredPer = 0
              var totalCODDelivered = 0
              var totalPPDDelivered = 0
              var totalCODDeliveredPer = 0
              var totalPPDDeliveredPer = 0
            }

          } else {

            var totalDelivered = 0
            var totalDeliveredPer = 0
            var totalCODDelivered = 0
            var totalPPDDelivered = 0
            var totalCODDeliveredPer = 0
            var totalPPDDeliveredPer = 0

          }
          await myArray.push({
            mobileno: element.mobileno,
            delivery_pincode: element.delivery_pincode,
            zone: element.zone,
            address: element.address,
            totalCOD: element.totalCOD,
            totalPPD: element.totalPPD,
            totalDelivered: totalDelivered,
            totalDeliveredPer: totalDeliveredPer,
            totalCODDelivered: totalCODDelivered,
            totalPPDDelivered: totalPPDDelivered,
            totalCODDeliveredPer: totalCODDeliveredPer,
            totalPPDDeliveredPer: totalPPDDeliveredPer,
            total_shipments: element.total_shipments
          });
        } else {
          var objIndex = myArray.findIndex(obj => obj.delivery_pincode == element.delivery_pincode && obj.zone == element.zone && obj.mobileno == element.mobileno && obj.address == element.address);
          myArray[objIndex].mobileno = myArray[objIndex].mobileno,
            myArray[objIndex].delivery_pincode = myArray[objIndex].delivery_pincode,
            myArray[objIndex].zone = myArray[objIndex].zone,
            myArray[objIndex].address = myArray[objIndex].address,
            myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
            myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
          myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
          if (element.dispatch_status == '5') {
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = (myArray[objIndex].totalDelivered + element.total_shipments)
              // console.log("@@@2",element.delivery_pincode,element.zone, myArray[objIndex].totalDelivered , myArray[objIndex].total_shipments)

              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered + element.totalCOD
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered + element.totalPPD

              if (myArray[objIndex].totalCODDelivered != 0) {
                // console.log("totalCODDelivered1---", element.delivery_pincode ,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
                myArray[objIndex].totalCODDeliveredPer = (myArray[objIndex].totalCODDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalCODDeliveredPer = 0
                // console.log("totalCODDelivered2---" ,element.delivery_pincode,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)

              }

              if (myArray[objIndex].totalPPDDelivered != 0) {
                myArray[objIndex].totalPPDDeliveredPer = (myArray[objIndex].totalPPDDelivered / myArray[objIndex].totalDelivered) * 100
              } else {
                myArray[objIndex].totalPPDDeliveredPer = 0
              }

            } else {
              myArray[objIndex].totalDelivered = 0
              myArray[objIndex].totalDeliveredPer = 0
              myArray[objIndex].totalCODDelivered = 0
              myArray[objIndex].totalPPDDelivered = 0
              myArray[objIndex].totalCODDeliveredPer = 0
              myArray[objIndex].totalPPDDeliveredPer = 0
            }

          } else {
            myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
            myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
              myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
            if (myArray[objIndex].total_shipments != 0) {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
              myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            } else {
              myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
              myArray[objIndex].totalDeliveredPer = myArray[objIndex].totalDeliveredPer
              myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
              myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
              myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
              myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
            }


            // if (myArray[objIndex].total_shipments != 0) {


          }
        }

      }
      console.log("Success")
      
    } else {
      console.log("no data")
     
    }
  return myArray
}


exports.getPincodePerformance = async (req, res, next) => {
  try{
    const pincode_performance = await pincode_performance_model.find({})
    if(pincode_performance){
      res.json({
        status: 200,
        data: pincode_performance
      })
    }else{
      res.json({
        status: 400,
        data: pincode_performance
      })
    }
  }catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
}

exports.getCustomerProfile = async (req, res, next) => {
  try {
    if (req.body.mobileno && (req.body.mobileno != '' || req.body.mobileno != undefined || req.body.mobileno != 'undefined') &&
      req.body.address && (req.body.address != '' || req.body.address != undefined || req.body.address != 'undefined') &&
      req.body.zone && (req.body.zone != '' || req.body.zone != undefined || req.body.zone != 'undefined') &&
      req.body.pincode && (req.body.pincode != '' || req.body.pincode != undefined || req.body.pincode != 'undefined') &&
      req.body.pay_type && (req.body.pay_type != '' || req.body.pay_type != undefined || req.body.pay_type != 'undefined')) {

      console.log("req---", req.body)
      var mobileno = trim(req.body.mobileno);
      var address = trim(req.body.address);
      var zone = trim(req.body.zone);
      var pincode = trim(req.body.pincode);
      var pay_type = trim(req.body.pay_type);
      var customer_performance = []
      if (mobileno) {
        customer_performance = await customer_performance_model.find({
          mobileno: mobileno,
          zone: zone,
        })
      } else {
        customer_performance == []
      }

      console.log("customer_performance---", customer_performance.length)
      if (customer_performance != '') {
        var customer_type = 'Repeat';
        console.log("if part")
        const existingAddress = await commanController.getCustumProfile(address, zone, customer_type, mobileno, pay_type, pincode)
        console.log("existingAddress---", existingAddress)
        if (existingAddress != '') {
          res.json({
            status: 200,
            data: existingAddress
          })
        } else {
          res.json({
            status: 200,
            data: {
              "Delivery Possibility": "Low"
            }
          })
        }
      } else {
        console.log("else part")
        var customer_type = 'New';
        var addressLength = address.length
        console.log("addressLength---", addressLength)
        var addressData = []
        if (address) {
          addressData = await commanController.addressCheck(addressLength, customer_type)
        } else {
          addressData == []
        }
        console.log("addressData---", addressData.length)
        if (addressData.length != 0) {
          const pincode_performance = await pincode_performance_model.find({
            pincode: pincode,
            zone: zone

          })
          // console.log("pincode_performance",pincode_performance)
          if (pincode_performance.length != 0) {
            for (let element of pincode_performance) {
              console.log("total.delivered_per---", element.total_delivered_per)
              const pincodeData = await commanController.pincodePerform(addressLength, element.total_delivered_per, zone, customer_type)
              if (pincodeData != '') {
                res.json({
                  status: 200,
                  data: pincodeData
                })
              } else {
                res.json({
                  status: 200,
                  data: {
                    "Delivery Possibility": "Low"
                  }
                })
              }
            }
          } else {
            res.json({
              status: 200,
              data: {
                "Delivery Possibility": "Low"
              }
            })
            // }
          }

        } else {
          console.log("@@@@addressless", addressLength, zone, customer_type)
          const lessAddress = await commanController.lessAddress(addressLength, zone, customer_type)
          console.log("lessaddress---", lessAddress)
          if (lessAddress != '') {
            res.json({
              status: 200,
              data: lessAddress
            })
          } else {
            res.json({
              status: 200,
              data: {
                "Delivery Possibility": "Low"
              }
            })
          }

        }


      }
    } else {
      res.json({
        status: 200,
        data: {
          "Delivery Possibility": "Low"
        }
      })
    }

  } catch (error) {
    console.log("error--", error)
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
}

exports.getCustomerProfile2 = async (req, res, next) => {
  console.log("getCustomerProfile2 reached")
  try {
    if (req.body.mobileno && (req.body.mobileno != '' || req.body.mobileno != undefined || req.body.mobileno != 'undefined') &&
      req.body.address && (req.body.address != '' || req.body.address != undefined || req.body.address != 'undefined') &&
      req.body.zone && (req.body.zone != '' || req.body.zone != undefined || req.body.zone != 'undefined') &&
      req.body.pincode && (req.body.pincode != '' || req.body.pincode != undefined || req.body.pincode != 'undefined') &&
      req.body.pay_type && (req.body.pay_type != '' || req.body.pay_type != undefined || req.body.pay_type != 'undefined')) {

      console.log("req---", req.body)
      var mobileno = trim(req.body.mobileno);
      var address = trim(req.body.address);
      var zone = trim(req.body.zone);
      var pincode = trim(req.body.pincode);
      var pay_type = trim(req.body.pay_type);
      var customer_performance = []
      if (mobileno) {
        customer_performance = await customer_performance_date_model.find({
          mobileno: mobileno,
          zone: zone,
        })
      } else {
        customer_performance == []
      }

      console.log("customer_performance---", customer_performance.length)
      if (customer_performance != '') {
        var customer_type = 'Repeat';
        console.log("if part")
        const existingAddress = await commanController.getCustumProfile2(address, zone, customer_type, mobileno, pay_type, pincode)
        console.log("existingAddress---", existingAddress)
        if (existingAddress != '') {
          res.json({
            status: 200,
            data: existingAddress
          })
        } else {
          res.json({
            status: 200,
            data: {
              "Delivery Possibility": "Low"
            }
          })
        }
      } else {
        console.log("else part")
        var customer_type = 'New';
        var addressLength = address.length
        console.log("addressLength---", addressLength)
        var addressData = []
        if (address) {
          addressData = await commanController.addressCheck(addressLength, customer_type)
        } else {
          addressData == []
        }
        console.log("addressData---", addressData.length)
        if (addressData.length != 0) {
          const pincode_performance = await pincode_performance_date_model.find({
            pincode: pincode,
            zone: zone

          })
          // console.log("pincode_performance",pincode_performance)
          if (pincode_performance.length != 0) {
            for (let element of pincode_performance) {
              console.log("total.delivered_per---", element.total_delivered_per)
              const pincodeData = await commanController.pincodePerform(addressLength, element.total_delivered_per, zone, customer_type)
              if (pincodeData != '') {
                res.json({
                  status: 200,
                  data: pincodeData
                })
              } else {
                res.json({
                  status: 200,
                  data: {
                    "Delivery Possibility": "Low"
                  }
                })
              }
            }
          } else {
            res.json({
              status: 200,
              data: {
                "Delivery Possibility": "Low"
              }
            })
            // }
          }

        } else {
          console.log("@@@@addressless", addressLength, zone, customer_type)
          const lessAddress = await commanController.lessAddress(addressLength, zone, customer_type)
          console.log("lessaddress---", lessAddress)
          if (lessAddress != '') {
            res.json({
              status: 200,
              data: lessAddress
            })
          } else {
            res.json({
              status: 200,
              data: {
                "Delivery Possibility": "Low"
              }
            })
          }

        }


      }
    } else {
      res.json({
        status: 200,
        data: {
          "Delivery Possibility": "Low"
        }
      })
    }

  } catch (error) {
    console.log("error--", error)
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
}

exports.getCustomerProfileData = async (req, res, next) => {

  try {
    if (req.body.mobileno && (req.body.mobileno != '' || req.body.mobileno != undefined || req.body.mobileno != 'undefined') &&
      req.body.address && (req.body.address != '' || req.body.address != undefined || req.body.address != 'undefined') &&
      req.body.zone && (req.body.zone != '' || req.body.zone != undefined || req.body.zone != 'undefined') &&
      req.body.pincode && (req.body.pincode != '' || req.body.pincode != undefined || req.body.pincode != 'undefined') &&
      req.body.pay_type && (req.body.pay_type != '' || req.body.pay_type != undefined || req.body.pay_type != 'undefined')) {
      var mobileno = trim(req.body.mobileno);
      var address = trim(req.body.address);
      var zone = trim(req.body.zone);
      var pincode = trim(req.body.pincode);
      var pay_type = trim(req.body.pay_type);
      var pickupData = [];
      var pincode_zones_data = [];
      var customerPerformance = [];
      var newArray = [];
      var myArray = [];
      var response = [];
      var addressData = [];
      var pincodePerformance = [];
      var pincodeData = [];
      pickupData = await pickupDao.getPickupData(mobileno)
      console.log("pickupData---", pickupData)
      if (pickupData != '') {
        console.log("repeat")
        var customer_type = 'Repeat';
        customerPerformance = await pickupDao.getMobileAndPincodeData(mobileno, pincode)
        // console.log("customerPerformance---",customerPerformance)
        for await (let element of customerPerformance) {
          pincode_zones_data = await commanController.getZonesData(element.sub_vendor_pincode, element.pincode, element.airwaybilno, element.id, element.telephone1, element.dispatch_status, element.product, element.consignee_email, element.consignee_address1, element.product_value, element.collectable_value)
          if (pincode_zones_data != '' && pincode_zones_data != undefined) {
            console.log("pincode_zones_data---",pincode_zones_data)

            for await (let data of pincode_zones_data) {
              var found =  newArray.some(el => el.delivery_pincode === data.delivery_pincode && el.zone === data.zone && el.dispatch_status === data.dispatch_status && el.mobileno === data.mobileno && el.address === data.address);
              console.log("found--", found)
              var totalCOD = 0;
              var totalPPD = 0;
              var total_shipments = 0;
              var paytype = '';
              if (data.collect_value > 0 && data.pay_type != 'COD') {
                paytype = 'COD'
              } else if (data.collect_value > 0 && data.pay_type == 'COD') {
                paytype = 'COD'
              }

              if (data.collect_value <= 0 && data.pay_type != 'PPD') {
                paytype = 'PPD'
              } else if (data.collect_value <= 0 && data.pay_type == 'PPD') {
                paytype = 'PPD'
              }

              if (found == false) {
                console.log("if part")
                if (paytype == 'COD') {
                  await newArray.push({
                    mobileno: data.mobileno,
                    delivery_pincode: data.delivery_pincode,
                    zone: data.zone,
                    address: data.address,
                    dispatch_status: data.dispatch_status,
                    totalCOD: total_shipments + 1,
                    totalPPD: totalPPD,
                    total_shipments: total_shipments + 1
                  });
                } else if (paytype == 'PPD') {
                  await newArray.push({
                    mobileno: data.mobileno,
                    delivery_pincode: data.delivery_pincode,
                    zone: data.zone,
                    address: data.address,
                    dispatch_status: data.dispatch_status,
                    totalCOD: totalCOD,
                    totalPPD: total_shipments + 1,
                    total_shipments: total_shipments + 1
                  });
                }

              } else {
                console.log("else part")
                if (paytype == 'COD') {
                  var objIndex = newArray.findIndex((obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status && obj.mobileno == data.mobileno && obj.address == data.address));
                  newArray[objIndex].mobileno = data.mobileno,
                    newArray[objIndex].delivery_pincode = data.delivery_pincode,
                    newArray[objIndex].zone = data.zone,
                    newArray[objIndex].address = data.address,
                    newArray[objIndex].dispatch_status = data.dispatch_status,
                    newArray[objIndex].totalCOD = newArray[objIndex].totalCOD + 1,
                    newArray[objIndex].totalPPD = newArray[objIndex].totalPPD
                  newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + 1

                } else if (paytype == 'PPD') {
                  var objIndex = newArray.findIndex(obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status && obj.mobileno == data.mobileno && obj.address == data.address);
                  newArray[objIndex].mobileno = data.mobileno,
                    newArray[objIndex].delivery_pincode = data.delivery_pincode,
                    newArray[objIndex].zone = data.zone,
                    newArray[objIndex].address = data.address,
                    newArray[objIndex].dispatch_status = data.dispatch_status,
                    newArray[objIndex].totalCOD = newArray[objIndex].totalCOD,
                    newArray[objIndex].totalPPD = newArray[objIndex].totalPPD + 1,
                    newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + 1
                }

              }
            }     
          }else{
            myArray = myArray
          }
        }

        console.log("newArray---",newArray)
        if(newArray != ''){
          for await (let element of newArray) {
            var found1 =  myArray.some(el => el.delivery_pincode === element.delivery_pincode && el.zone === element.zone && el.mobileno == element.mobileno && el.address == element.address);
            if (found1 == false) {
              console.log("found1---",found1, element.delivery_pincode, element.zone)
   
              if (element.dispatch_status == '5') {
                if (element.total_shipments != 0) {
                  var totalDelivered = element.total_shipments
                  // console.log("@@@1",element.delivery_pincode,element.zone, element.total_shipments , element.total_shipments)
                  var totalDeliveredPer = (element.total_shipments / element.total_shipments) * 100
                  var totalCODDelivered = element.totalCOD
                  var totalPPDDelivered = element.totalPPD
                  if (element.totalCOD != 0) {
                    var totalCODDeliveredPer = (element.totalCOD / element.total_shipments) * 100
                  } else {
                    var totalCODDeliveredPer = 0
                  }
   
                  if (element.totalPPD != 0) {
                    var totalPPDDeliveredPer = (element.totalPPD / element.total_shipments) * 100
                  } else {
                    var totalPPDDeliveredPer = 0
                  }
                  // console.log("@@@1", totalCODDeliveredPer)
                } else {
                  var totalDelivered = 0
                  var totalDeliveredPer = 0
                  var totalCODDelivered = 0
                  var totalPPDDelivered = 0
                  var totalCODDeliveredPer = 0
                  var totalPPDDeliveredPer = 0
                }
   
              } else {
   
                var totalDelivered = 0
                var totalDeliveredPer = 0
                var totalCODDelivered = 0
                var totalPPDDelivered = 0
                var totalCODDeliveredPer = 0
                var totalPPDDeliveredPer = 0
   
              }
              await myArray.push({
                mobileno: element.mobileno,
                delivery_pincode: element.delivery_pincode,
                zone: element.zone,
                address: element.address,
                totalCOD: element.totalCOD,
                totalPPD: element.totalPPD,
                totalDelivered: totalDelivered,
                totalDeliveredPer: totalDeliveredPer,
                totalCODDelivered: totalCODDelivered,
                totalPPDDelivered: totalPPDDelivered,
                totalCODDeliveredPer: totalCODDeliveredPer,
                totalPPDDeliveredPer: totalPPDDeliveredPer,
                total_shipments: element.total_shipments
              });
            } else {
              console.log("found1---",found1, element.delivery_pincode, element.zone )
              var objIndex = myArray.findIndex(obj => obj.delivery_pincode == element.delivery_pincode && obj.zone == element.zone && obj.mobileno == element.mobileno && obj.address == element.address);
              myArray[objIndex].mobileno = myArray[objIndex].mobileno,
                myArray[objIndex].delivery_pincode = myArray[objIndex].delivery_pincode,
                myArray[objIndex].zone = myArray[objIndex].zone,
                myArray[objIndex].address = myArray[objIndex].address,
                myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
                myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
                console.log("myArray[objIndex].total_shipments---",myArray[objIndex].total_shipments)
                console.log("element.total_shipments---",element.total_shipments)
                console.log("myArray[objIndex].total_shipments + element.total_shipments---",myArray[objIndex].total_shipments + element.total_shipments)
              myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
              if (element.dispatch_status == '5') {
                if (myArray[objIndex].total_shipments != 0) {
                  myArray[objIndex].totalDelivered = (myArray[objIndex].totalDelivered + element.total_shipments)
                  // console.log("@@@2",element.delivery_pincode,element.zone, myArray[objIndex].totalDelivered , myArray[objIndex].total_shipments)
   
                  myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
                  myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered + element.totalCOD
                  myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered + element.totalPPD
   
                  if (myArray[objIndex].totalCODDelivered != 0) {
                    // console.log("totalCODDelivered1---", element.delivery_pincode ,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
                    myArray[objIndex].totalCODDeliveredPer = (myArray[objIndex].totalCODDelivered / myArray[objIndex].totalDelivered) * 100
                  } else {
                    myArray[objIndex].totalCODDeliveredPer = 0
                    // console.log("totalCODDelivered2---" ,element.delivery_pincode,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
   
                  }
   
                  if (myArray[objIndex].totalPPDDelivered != 0) {
                    myArray[objIndex].totalPPDDeliveredPer = (myArray[objIndex].totalPPDDelivered / myArray[objIndex].totalDelivered) * 100
                  } else {
                    myArray[objIndex].totalPPDDeliveredPer = 0
                  }
   
                } else {
                  myArray[objIndex].totalDelivered = 0
                  myArray[objIndex].totalDeliveredPer = 0
                  myArray[objIndex].totalCODDelivered = 0
                  myArray[objIndex].totalPPDDelivered = 0
                  myArray[objIndex].totalCODDeliveredPer = 0
                  myArray[objIndex].totalPPDDeliveredPer = 0
                }
   
              } else {
                myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
                myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
                  myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
                if (myArray[objIndex].total_shipments != 0) {
                  myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
                  myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
                  myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
                  myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
                  myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
                  myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
                } else {
                  myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
                  myArray[objIndex].totalDeliveredPer = myArray[objIndex].totalDeliveredPer
                  myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
                  myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
                  myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
                  myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
                }
   
              }
            }
   
   
          }
        }else{
          myArray = myArray
        }
        
        console.log("myArray---",myArray)
        response = await commanController.getRepeatProfile(address, zone, customer_type, mobileno, pay_type, pincode, myArray)
        console.log("response---", response)
        if (response != '') {
          res.json({
            status: 200,
            data: response
          })
        } else {
          res.json({
            status: 200,
            data: {
              "Delivery Possibility": "Low"
            }
          })
        }
      } else {
        console.log("else part")
        var customer_type = 'New';
        var addressLength = address.length
        console.log("addressLength--", addressLength)
        if (address) {
          addressData = await commanController.addressCheck(addressLength, customer_type)
        } else {
          addressData == []
        }
        // console.log("addressData---", addressData.length)
        if (addressData.length != 0) {
          pincodePerformance = await pickupDao.getPincodeData(pincode)
          for await (let element of pincodePerformance) {
            pincode_zones_data = await commanController.getZonesData(element.sub_vendor_pincode, element.pincode, element.airwaybilno, element.id, element.telephone1, element.dispatch_status, element.product, element.consignee_email, element.consignee_address1, element.product_value, element.collectable_value)
            // console.log("pincode_zones_data---",pincode_zones_data)
            if (pincode_zones_data != '' && pincode_zones_data != undefined) {
              for await (let data of pincode_zones_data) {
                var found = await newArray.some(el => el.delivery_pincode === data.delivery_pincode && el.zone === data.zone && el.dispatch_status === data.dispatch_status);
                
                var totalCOD = 0;
                var totalPPD = 0;
                var total_shipments = 0;
                var paytype = '';
                if (data.collect_value > 0 && data.pay_type != 'COD') {
                  paytype = 'COD'
                } else if (data.collect_value > 0 && data.pay_type == 'COD') {
                  paytype = 'COD'
                }
        
                if (data.collect_value <= 0 && data.pay_type != 'PPD') {
                  paytype = 'PPD'
                } else if (data.collect_value <= 0 && data.pay_type == 'PPD') {
                  paytype = 'PPD'
                }
                if (found == false) {
                  console.log("found---",found)
                  if (paytype == 'COD') {
                    console.log("if")
                    console.log("paytype---",paytype)
                    console.log("data.zone---",data.zone)
                    console.log("total_shipments---",total_shipments +1)
                    await newArray.push({
                      delivery_pincode: data.delivery_pincode,
                      zone: data.zone,
                      dispatch_status: data.dispatch_status,
                      totalCOD: total_shipments + 1,
                      totalPPD: totalPPD,
                      total_shipments: total_shipments + 1
                    });
                  } else if (paytype == 'PPD') {
                    console.log("else if")
                    console.log("paytype---",paytype)
                    console.log("data.zone---",data.zone)
                    console.log("total_shipments---",total_shipments +1)
                    await newArray.push({
                      delivery_pincode: data.delivery_pincode,
                      zone: data.zone,
                      dispatch_status: data.dispatch_status,
                      totalCOD: totalCOD,
                      totalPPD: total_shipments + 1,
                      total_shipments: total_shipments + 1
                    });
                  }

                } else {
                  console.log("found---",found)
                  if (paytype == 'COD') {
                    console.log("else if---")
                    console.log("paytype---",paytype)
                    console.log("data.zone---",data.zone)
                    console.log("total_shipments---",total_shipments +1)
                    var objIndex = newArray.findIndex((obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status));
                    newArray[objIndex].delivery_pincode = data.delivery_pincode,
                      newArray[objIndex].zone = data.zone,
                      newArray[objIndex].dispatch_status = data.dispatch_status,
                      newArray[objIndex].totalCOD = newArray[objIndex].totalCOD + 1,
                      newArray[objIndex].totalPPD = newArray[objIndex].totalPPD
                    newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + 1

                  } else if (paytype == 'PPD') {
                    console.log("else ---else if")
                    console.log("paytype---",paytype)
                    console.log("data.zone---",data.zone)
                    console.log("total_shipments---",total_shipments +1)
                    var objIndex = newArray.findIndex(obj => obj.delivery_pincode == data.delivery_pincode && obj.zone == data.zone && obj.dispatch_status == data.dispatch_status);
                    newArray[objIndex].delivery_pincode = data.delivery_pincode,
                      newArray[objIndex].zone = data.zone,
                      newArray[objIndex].dispatch_status = data.dispatch_status,
                      newArray[objIndex].totalCOD = newArray[objIndex].totalCOD,
                      newArray[objIndex].totalPPD = newArray[objIndex].totalPPD + 1,
                      newArray[objIndex].total_shipments = newArray[objIndex].total_shipments + 1
                  }

                }
              }
              // console.log("newArray---",newArray)
            } else {
              myArray = myArray
            }
          }
          if(newArray != ''){
            for await (let element of newArray) {
              var found1 = await myArray.some(el => el.delivery_pincode === element.delivery_pincode && el.zone === element.zone);
              if (found1 == false) {
                // console.log("found1---",found1,element.zone,element.total_shipments,element.dispatch_status,element.delivery_pincode)
                if (element.dispatch_status == '5') {
                  if (element.total_shipments != 0) {
                    var totalDelivered = element.total_shipments
                    // console.log("@@@1",element.delivery_pincode,element.zone, element.total_shipments , element.total_shipments)
                    var totalDeliveredPer = (element.total_shipments / element.total_shipments) * 100
                    var totalCODDelivered = element.totalCOD
                    var totalPPDDelivered = element.totalPPD
                    if (element.totalCOD != 0) {
                      var totalCODDeliveredPer = (element.totalCOD / element.total_shipments) * 100
                    } else {
                      var totalCODDeliveredPer = 0
                    }

                    if (element.totalPPD != 0) {
                      var totalPPDDeliveredPer = (element.totalPPD / element.total_shipments) * 100
                    } else {
                      var totalPPDDeliveredPer = 0
                    }
                    // console.log("@@@1", totalCODDeliveredPer)
                  } else {
                    var totalDelivered = 0
                    var totalDeliveredPer = 0
                    var totalCODDelivered = 0
                    var totalPPDDelivered = 0
                    var totalCODDeliveredPer = 0
                    var totalPPDDeliveredPer = 0
                  }

                } else {

                  var totalDelivered = 0
                  var totalDeliveredPer = 0
                  var totalCODDelivered = 0
                  var totalPPDDelivered = 0
                  var totalCODDeliveredPer = 0
                  var totalPPDDeliveredPer = 0

                }
                await myArray.push({
                  delivery_pincode: element.delivery_pincode,
                  zone: element.zone,
                  totalCOD: element.totalCOD,
                  totalPPD: element.totalPPD,
                  totalDelivered: totalDelivered,
                  totalDeliveredPer: totalDeliveredPer,
                  totalCODDelivered: totalCODDelivered,
                  totalPPDDelivered: totalPPDDelivered,
                  totalCODDeliveredPer: totalCODDeliveredPer,
                  totalPPDDeliveredPer: totalPPDDeliveredPer,
                  total_shipments: element.total_shipments
                });
                // console.log("###########---",myArray)
              } else {
                // console.log("found1---",found1,element.zone,element.total_shipments,element.dispatch_status, element.delivery_pincode)

                var objIndex = myArray.findIndex(obj => obj.delivery_pincode == element.delivery_pincode && obj.zone == element.zone);
                myArray[objIndex].delivery_pincode = myArray[objIndex].delivery_pincode,
                  myArray[objIndex].zone = myArray[objIndex].zone,
                  myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
                  myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
                myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
                if (element.dispatch_status == '5') {
                  if (myArray[objIndex].total_shipments != 0) {
                    myArray[objIndex].totalDelivered = (myArray[objIndex].totalDelivered + element.total_shipments)
                    // console.log("@@@2",element.delivery_pincode,element.zone, myArray[objIndex].totalDelivered , myArray[objIndex].total_shipments)

                    myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
                    myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered + element.totalCOD
                    myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered + element.totalPPD

                    if (myArray[objIndex].totalCODDelivered != 0) {
                      // console.log("totalCODDelivered1---", element.delivery_pincode ,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)
                      myArray[objIndex].totalCODDeliveredPer = (myArray[objIndex].totalCODDelivered / myArray[objIndex].totalDelivered) * 100
                    } else {
                      myArray[objIndex].totalCODDeliveredPer = 0
                      // console.log("totalCODDelivered2---" ,element.delivery_pincode,myArray[objIndex].totalCODDelivered,myArray[objIndex].totalDelivered)

                    }

                    if (myArray[objIndex].totalPPDDelivered != 0) {
                      myArray[objIndex].totalPPDDeliveredPer = (myArray[objIndex].totalPPDDelivered / myArray[objIndex].totalDelivered) * 100
                    } else {
                      myArray[objIndex].totalPPDDeliveredPer = 0
                    }

                  } else {
                    myArray[objIndex].totalDelivered = 0
                    myArray[objIndex].totalDeliveredPer = 0
                    myArray[objIndex].totalCODDelivered = 0
                    myArray[objIndex].totalPPDDelivered = 0
                    myArray[objIndex].totalCODDeliveredPer = 0
                    myArray[objIndex].totalPPDDeliveredPer = 0
                  }

                } else {
                  myArray[objIndex].total_shipments = myArray[objIndex].total_shipments + element.total_shipments
                  myArray[objIndex].totalCOD = myArray[objIndex].totalCOD + element.totalCOD,
                    myArray[objIndex].totalPPD = myArray[objIndex].totalPPD + element.totalPPD
                  if (myArray[objIndex].total_shipments != 0) {
                    myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
                    myArray[objIndex].totalDeliveredPer = (myArray[objIndex].totalDelivered / myArray[objIndex].total_shipments) * 100
                    myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
                    myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
                    myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
                    myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
                  } else {
                    myArray[objIndex].totalDelivered = myArray[objIndex].totalDelivered
                    myArray[objIndex].totalDeliveredPer = myArray[objIndex].totalDeliveredPer
                    myArray[objIndex].totalCODDelivered = myArray[objIndex].totalCODDelivered
                    myArray[objIndex].totalPPDDelivered = myArray[objIndex].totalPPDDelivered
                    myArray[objIndex].totalCODDeliveredPer = myArray[objIndex].totalCODDeliveredPer
                    myArray[objIndex].totalPPDDeliveredPer = myArray[objIndex].totalPPDDeliveredPer
                  }


                  // if (myArray[objIndex].total_shipments != 0) {


                }
              }

            }
          }else{
            myArray = myArray
          }
         console.log("myArray---",myArray)
         pincodeData = await commanController.getPincodeZoneData(pincode, zone, myArray)
        //  console.log("pincodeData---",pincodeData)
         if(pincodeData != ''){
          for (let element of pincodeData) {
            console.log("total.delivered_per---", element.totalDeliveredPer)
            const pincodeData = await commanController.pincodePerform(addressLength, element.totalDeliveredPer, zone, customer_type)
            if (pincodeData != '') {
              res.json({
                status: 200,
                data: pincodeData
              })
            } else {
              res.json({
                status: 200,
                data: {
                  "Delivery Possibility": "Low"
                }
              })
            }
          }
         }else{
          res.json({
            status: 200,
            data: {
              "Delivery Possibility": "Low"
            }
          })
         }
        } else {
          console.log("@@@@addressless", addressLength, zone, customer_type)
          const lessAddress = await commanController.lessAddress(addressLength, zone, customer_type)
          console.log("lessaddress---", lessAddress)
          if (lessAddress != '') {
            res.json({
              status: 200,
              data: lessAddress
            })
          } else {
            res.json({
              status: 200,
              data: {
                "Delivery Possibility": "Low"
              }
            })
          }
        }
      }

    } else {
      res.json({
        status: 200,
        data: {
          "Delivery Possibility": "Low"
        }
      })
    }


  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Some error occurred while creating pincode_zones."
    });
  }
}