const addressModel = require('../models/address_masters');
const pincodePerformanceModel = require('../models/pincode_performance_masters');
const orderHistoryModel = require('../models/order_history_masters');
const payTypeModel = require('../models/paytype_masters');
const orderStatusModel = require('../models/order_status_masters');
const deliveryPossibilityModel = require('../models/delivery_possibility_masters');
const flagModel = require('../models/flag_masters');
const recommendationModel = require('../models/recommendation');
const customerTypeModel = require('../models/customer_type_masters');
const customerHistoryModel = require('../models/customer_history_masters');
const zoneModel = require('../models/zone_masters');
var trim = require('trim');



exports.saveRecommendation = async (req, res, next) => {
    // console.log("req.body---",req.body)
    try {
        if (req.body.customer_type && (req.body.customer_type !== undefined || req.body.customer_type !== 'undefined' || req.body.customer_type !== '') &&
            req.body.customer_history && (req.body.customer_history !== undefined || req.body.customer_history !== 'undefined' || req.body.customer_history !== '') &&
            req.body.zone && (req.body.zone !== undefined || req.body.zone !== 'undefined' || req.body.zone !== '') &&
            (req.body.address !== undefined || req.body.address !== 'undefined') &&
            (req.body.address_length !== undefined || req.body.address_length !== 'undefined') &&
            (req.body.pincode_performance !== undefined || req.body.pincode_performance !== 'undefined') &&
            (req.body.pincode_greater !== undefined || req.body.pincode_greater !== 'undefined') &&
            (req.body.pincode_less !== undefined || req.body.pincode_less !== 'undefined') &&
            (req.body.order_history !== undefined || req.body.order_history !== 'undefined') &&
            (req.body.order_history_greater !== undefined || req.body.order_history_greater !== 'undefined') &&
            req.body.pay_type && (req.body.pay_type !== undefined || req.body.pay_type !== 'undefined' || req.body.pay_type !== '') &&
            (req.body.order_status !== undefined || req.body.order_status !== 'undefined') &&
            (req.body.statusGreater !== undefined || req.body.statusGreater !== 'undefined') &&
            (req.body.statusLess !== undefined || req.body.statusLess !== 'undefined') &&
            req.body.delivery_possibility && (req.body.delivery_possibility !== undefined || req.body.delivery_possibility !== 'undefined' || req.body.delivery_possibility !== '') &&
            req.body.flag && (req.body.flag !== undefined || req.body.flag !== 'undefined' || req.body.flag !== '')) {
            var customer_type = trim(req.body.customer_type)
            var customer_history = trim(req.body.customer_history)
            var zone = trim(req.body.zone)
            var address = trim(req.body.address)
            var pincode_performance = trim(req.body.pincode_performance)
            var order_history = trim(req.body.order_history)
            var pay_type = trim(req.body.pay_type)
            var order_status = trim(req.body.order_status)
            var delivery_possibility = trim(req.body.delivery_possibility)
            var flag = trim(req.body.flag)
            var address_length = req.body.address_length
            var pincode_greater = req.body.pincode_greater
            var pincode_less = req.body.pincode_less
            var status_greater = req.body.statusGreater
            var status_less = req.body.statusLess
            var order_history_greater = req.body.order_history_greater
            
            var alreadyExist = await recommendationModel.find({
                customer_type: customer_type,
                customer_history: customer_history,
                zone: zone,
                address: address,
                pincode_performance: pincode_performance,
                order_history: order_history,
                pay_type: pay_type,
                order_status: order_status,
                delivery_possibility: delivery_possibility,
                flag: flag,
                address_length: address_length,
                pincode_greater: pincode_greater,
                pincode_less: pincode_less,
                status_greater: status_greater,
                status_less: status_less,
                order_history_greater: order_history_greater
            })
            // console.log("alreadyExist---",alreadyExist)
            if(alreadyExist != ''){
                res.json({
                    status: 201,
                    message: 'Already existing this rule..'
                })
            }else{
                var recommendation = new recommendationModel({
                    customer_type: customer_type,
                    customer_history: customer_history,
                    zone: zone,
                    address: address,
                    pincode_performance: pincode_performance,
                    order_history: order_history,
                    pay_type: pay_type,
                    order_status: order_status,
                    delivery_possibility: delivery_possibility,
                    flag: flag,
                    address_length: address_length,
                    pincode_greater: pincode_greater,
                    pincode_less: pincode_less,
                    status_greater: status_greater,
                    status_less: status_less,
                    order_history_greater: order_history_greater
    
                });
                recommendation.save(function (err, result) {
                    if (err) {
                        res.json({
                            status: 401,
                            message: 'Some Error Occured During Recommendation Creation.'
                        })
                    } else {
                        res.json({
                            status: 200,
                            message: 'Recommendation Save Successfully'
                        });
                    }
                });
            }
        } else {
            res.json({
                status: 400,
                message: 'All fields are required..'
            })
        }
    } catch (error) {
        console.log("error---",error)
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }


}

exports.updateRecommendation = async (req, res, next) => {
    try {
        if (req.body.customer_type && (req.body.customer_type !== undefined || req.body.customer_type !== 'undefined' || req.body.customer_type !== '') &&
            req.body.customer_history && (req.body.customer_history !== undefined || req.body.customer_history !== 'undefined' || req.body.customer_history !== '') &&
            req.body.zone && (req.body.zone !== undefined || req.body.zone !== 'undefined' || req.body.zone !== '') &&
            (req.body.address !== undefined || req.body.address !== 'undefined') &&
            (req.body.address_length !== undefined || req.body.address_length !== 'undefined') &&
            (req.body.pincode_performance !== undefined || req.body.pincode_performance !== 'undefined') &&
            (req.body.pincode_greater !== undefined || req.body.pincode_greater !== 'undefined') &&
            (req.body.pincode_less !== undefined || req.body.pincode_less !== 'undefined') &&
            (req.body.order_history !== undefined || req.body.order_history !== 'undefined') &&
            (req.body.order_history_greater !== undefined || req.body.order_history_greater !== 'undefined') &&
            req.body.pay_type && (req.body.pay_type !== undefined || req.body.pay_type !== 'undefined' || req.body.pay_type !== '') &&
            (req.body.order_status !== undefined || req.body.order_status !== 'undefined') &&
            (req.body.statusGreater !== undefined || req.body.statusGreater !== 'undefined') &&
            (req.body.statusLess !== undefined || req.body.statusLess !== 'undefined') &&
            req.body.delivery_possibility && (req.body.delivery_possibility !== undefined || req.body.delivery_possibility !== 'undefined' || req.body.delivery_possibility !== '') &&
            req.body.flag && (req.body.flag !== undefined || req.body.flag !== 'undefined' || req.body.flag !== '') &&
            req.body._id && (req.body._id !== undefined || req.body._id !== 'undefined' || req.body._id !== '')) {
            var _id = trim(req.body._id)
            var customer_type = trim(req.body.customer_type)
            var customer_history = trim(req.body.customer_history)
            var zone = trim(req.body.zone)
            var address = trim(req.body.address)
            var pincode_performance = trim(req.body.pincode_performance)
            var order_history = trim(req.body.order_history)
            var pay_type = trim(req.body.pay_type)
            var order_status = trim(req.body.order_status)
            var delivery_possibility = trim(req.body.delivery_possibility)
            var flag = trim(req.body.flag)
            var address_length = req.body.address_length
            var pincode_greater = req.body.pincode_greater
            var pincode_less = req.body.pincode_less
            var status_greater = req.body.statusGreater
            var status_less = req.body.statusLess
            var order_history_greater = req.body.order_history_greater

            var alreadyExist = await recommendationModel.find({
                customer_type: customer_type,
                customer_history: customer_history,
                zone: zone,
                address: address,
                pincode_performance: pincode_performance,
                order_history: order_history,
                pay_type: pay_type,
                order_status: order_status,
                delivery_possibility: delivery_possibility,
                flag: flag,
                address_length: address_length,
                pincode_greater: pincode_greater,
                pincode_less: pincode_less,
                status_greater: status_greater,
                status_less: status_less,
                order_history_greater: order_history_greater
            })
            // console.log("alreadyExist---",alreadyExist)
            if(alreadyExist != ''){
                res.json({
                    status: 201,
                    message: 'Please changes on it then update.'
                })
            }else{
                var query = {
                    _id: _id
                },
                    update = {
                        $set: {
                            customer_type: customer_type,
                            customer_history: customer_history,
                            zone: zone,
                            address: address,
                            pincode_performance: pincode_performance,
                            order_history: order_history,
                            pay_type: pay_type,
                            order_status: order_status,
                            delivery_possibility: delivery_possibility,
                            flag: flag,
                            address_length: address_length,
                            pincode_greater: pincode_greater,
                            pincode_less: pincode_less,
                            status_greater: status_greater,
                            status_less: status_less,
                            order_history_greater: order_history_greater
                        }
                    };
                recommendationModel.updateMany(query, update, function (err, recommendation) {
                    if (err) {
                        return res.status(401).json({
                            message: 'Bad Request'
                        });
                    } else {
                        res.json({
                            status: 200,
                            message: 'Recommendation updated successfully..'
                        })
                    }
    
                });
            }
        } else {
            res.json({
                status: 400,
                message: 'All fields are required..'
            })
        }

    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}


exports.getRecommendationConfig = async (req, res, next) => {
    try {
        var view_data = [];
        const recommendation = await recommendationModel.find({});
        if (recommendation != '') {
            // console.log("recommendation---",recommendation)
                res.json({
                status: 200,
                message: 'success',
                data: recommendation,
            })
        } else {
            res.json({
                status: 400,
                message: 'Not Data Found',
                data: recommendation
            })
        }
    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}

exports.deleteRecommendation = async (req, res, next) => {
    // console.log("req.query---",req.query)
    try {
        if (req.query._id && (req.query._id !== undefined || req.query._id !== 'undefined' || req.query._id !== '')) {
            var _id = trim(req.query._id)
            var removeData = []
            var query = {
                _id: _id,
            }
            removeData = await recommendationModel.find({
                _id: _id,
            })
            if (removeData.length != 0) {
                recommendationModel.findOneAndRemove(query).exec(function (err, recommendation) {
                    if (err) {
                        return res.status(500).json({
                            message: 'Internal server error!!!....'
                        });
                    } else {
                        res.json({
                            status: 200,
                            data: recommendation,
                            message: 'Deleted Successfully!!!....'
                        });
                    }

                });
            } else if (removeData.length == 0) {
                res.json({
                    status: 400,
                    message: 'Bad Request!!!....'
                });
            }
        } else {
            res.json({
                message: 'Select the rule you want to deleted!!!....'
            });
        }

    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}


exports.getRecommendation = async (req, res, next) => {
    try {
        if (req.query._id && (req.query._id !== undefined || req.query._id !== 'undefined' || req.query._id !== '')) {
            var _id = trim(req.query._id)
            const recommendation = await recommendationModel.find({ _id: _id });
            if (recommendation != '') {
                res.json({
                    status: 200,
                    message: 'success',
                    data: recommendation,
                })
            } else {
                res.json({
                    status: 400,
                    message: 'Not Data Found',
                    data: recommendation
                })
            }
        } else {
            res.json({
                message: 'Required _id field'
            });
        }

    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
}