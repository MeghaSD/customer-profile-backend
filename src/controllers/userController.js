const usersModel = require('../models/users');
const cipher = require('../middleware/ciperHelper');
const emailvalidator = require('email-validator');
var trim = require('trim');


exports.saveUser = async (req, res, next) => {
    try {
        if (req.body.email && (req.body.email !== undefined || req.body.email !== 'undefined' || req.body.email !== '') &&
            req.body.userName && (req.body.userName !== undefined || req.body.userName !== 'undefined' || req.body.userName !== '') &&
            req.body.password && (req.body.password !== undefined || req.body.password !== 'undefined' || req.body.password !== '')) {
            var email = trim(req.body.email)
            var userName = trim(req.body.userName)
            var password = trim(req.body.password)
            if (emailvalidator.validate(email)) {
                const { salt, passwordHash } = cipher.saltHashPassword(password);
                usersModel.findOneAndUpdate(
                    {
                        email: email,
                    },
                    {
                        $set: {
                            userName: userName,
                            password: password,
                            passwordHash: passwordHash,
                            salt: salt
                        }
                    },
                    { upsert: true, new: true },
                    function (err, users) {
                        if (err) {
                            res.json({
                                status: 400,
                                message: 'Some Error Occured During Creation.'
                            });
                        } else {
                            users.password = undefined;
                            users.passwordHash = undefined;
                            users.salt = undefined;
                            res.status(200).json({
                                status: "success",
                                data: {
                                    users,
                                },
                            });
                        }
                    });

            } else {
                res.json({
                    status: 400,
                    message: 'Invalid Email'

                })
            }

        } else {
            res.json({
                status: 400,
                message: 'All fields are required..'

            })
        }

    } catch (error) {
        return res.status(500).json({
            error: 'Internal Server Error'
        })
    }
};