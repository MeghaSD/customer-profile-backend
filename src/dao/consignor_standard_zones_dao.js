const database = require('../config/index');
const consignor_standard_billing_zones_alias = require('../models/consignor_standard_billing_zones_alias');
const sequelize = require('sequelize')
const system = database.system;

const getZone = consignor_standard_billing_zones_alias.getZone = function (zone_code) {
    var query ="select zone from consignor_standard_billing_zones_alias tz where tz.zone_code='" + zone_code + "'";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const customDeliveryList = consignor_standard_billing_zones_alias.customDeliveryList = function () {
    var query = "select * from consignor_standard_billing_zones_alias tz where tz.is_default=0 and tz.is_custom=1 and tz.is_delivery=0 and tz.is_deleted=0";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}


module.exports ={
    getZone,
    customDeliveryList
}