const database = require('../config/index');
const ilogix_standard_other_pincodes = require('../models/ilogix_standard_other_pincodes');
const sequelize = require('sequelize')
const system = database.system;

const getOtherPincodeZone = ilogix_standard_other_pincodes.getOtherPincodeZone = function (zone_code,deliveryPincode) {
    var query = "select * from ilogix_standard_other_pincodes tp where tp.zone_code= '" + zone_code + "' AND tp.is_deleted= 0 AND tp.pincode IN (" + deliveryPincode + ") limit 1";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const getOtherPincodeZone2 = ilogix_standard_other_pincodes.getOtherPincodeZone2 = function (zone_code,pickupPincode,deliveryPincode) {
    var query = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 10";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const getMetroZone = ilogix_standard_other_pincodes.getMetroZone = function (zone_code,pickupPincode,deliveryPincode) {
    var query = "select * from ilogix_standard_other_pincodes tp where tp.zone_code='" + zone_code + "' AND tp.is_deleted=0 AND tp.pincode IN (" + pickupPincode + "," + deliveryPincode + ") limit 2";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

const getRestOfIndiaZone = ilogix_standard_other_pincodes.getRestOfIndiaZone = function (zone_code,deliveryPincode) {
    var query = "select * from ilogix_standard_other_pincodes tp where tp.zone_code= '" + zone_code + "' AND tp.is_deleted= 0 AND tp.pincode IN (" + deliveryPincode + ") limit 2";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });
}

module.exports ={
    getOtherPincodeZone,
    getOtherPincodeZone2,
    getMetroZone,
    getRestOfIndiaZone
}