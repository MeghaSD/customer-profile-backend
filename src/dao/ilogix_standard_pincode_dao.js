const database = require('../config/index');
const ilogix_standard_pincodes_master = require('../models/ilogix_standard_pincodes_master');
const sequelize = require('sequelize')
const system = database.system;

const getIlogixStandardPincodes = ilogix_standard_pincodes_master.getIlogixStandardPincodes = function (pickupPincode,deliveryPincode) {
    var query = "select pm.*,pm.state as state_id,pm.city as city_id from ilogix_standard_pincodes_master pm where pm.pincode IN (" + pickupPincode + ',' + deliveryPincode + ")";
    return system.sequelize.query(query, { type: system.sequelize.QueryTypes.SELECT });

}

module.exports ={
    getIlogixStandardPincodes
}