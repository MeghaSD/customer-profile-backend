const database = require('../config/index');
const pickup = require('../models/pickup');
const sequelize = require('sequelize')
const landmark = database.landmark;

const getPickupsCount = pickup.getPickupsCount = function (tableName) {
    var query = "SELECT COUNT(*) as count FROM pickup WHERE dispatch_status in ('5','16','13','15')  AND NOT id IN (select pickup_id from "+tableName+")";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const getPickups = pickup.getPickups = function (tableName, limit) {
    var query = "SELECT * FROM pickup WHERE dispatch_status in ('5','16','13','15')  AND NOT id IN (select pickup_id from "+tableName+") limit "+limit+"";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const pincodePerformance = pickup.pincodePerformance = function () {
    var query = "SELECT COUNT(*), COUNT(pickup.id) as total, pincode_zones.delivery_pincode, pincode_zones.pay_type, pincode_zones.zone, pincode_zones.dispatch_status FROM pickup LEFT JOIN pincode_zones ON  pickup.id = pincode_zones.pickup_id GROUP BY pincode_zones.delivery_pincode, pincode_zones.pay_type, pincode_zones.zone, pincode_zones.dispatch_status";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const customerPerformance = pickup.customerPerformance = function () {
    var query = "SELECT COUNT(*), COUNT(pickup.id) as total, pincode_zones.delivery_pincode, pincode_zones.pay_type, pincode_zones.zone, pincode_zones.address, pincode_zones.mobileno, pincode_zones.dispatch_status FROM pickup LEFT JOIN pincode_zones ON  pickup.id = pincode_zones.pickup_id GROUP BY pincode_zones.delivery_pincode, pincode_zones.mobileno, pincode_zones.zone, pincode_zones.address";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const customerPerformance1 = pickup.customerPerformance1 = function (tableName) {
    var query = "SELECT COUNT(*), COUNT(pickup.id) as total, "+tableName+".delivery_pincode, "+tableName+".pay_type, "+tableName+".zone, "+tableName+".address, "+tableName+".mobileno, "+tableName+".dispatch_status FROM pickup LEFT JOIN "+tableName+" ON  pickup.id = "+tableName+".pickup_id GROUP BY "+tableName+".delivery_pincode, "+tableName+".mobileno, "+tableName+".zone, "+tableName+".address";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const pincodePerformance1 = pickup.pincodePerformance1 = function (tableName) {
    var query = "SELECT COUNT(*), COUNT(pickup.id) as total, "+tableName+".delivery_pincode, "+tableName+".pay_type, "+tableName+".zone, "+tableName+".dispatch_status FROM pickup LEFT JOIN "+tableName+" ON  pickup.id = "+tableName+".pickup_id GROUP BY "+tableName+".delivery_pincode, "+tableName+".pay_type, "+tableName+".zone, "+tableName+".dispatch_status";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const getPickupData = pickup.getPickupData = function (mobileno) {
    var query = "SELECT * FROM pickup WHERE dispatch_status in ('5','16','13','15')  AND telephone1 = '"+mobileno+"'";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const getMobileAndPincodeData = pickup.getMobileAndPincodeData = function (mobileno, pincode) {
    var query = "SELECT * FROM pickup WHERE dispatch_status in ('5','16','13','15') AND telephone1 = '"+mobileno+ "' AND pincode = '"+pincode+"'" ;
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

const getPincodeData = pickup.getPincodeData = function (pincode) {
    var query = "SELECT * FROM pickup WHERE dispatch_status in ('5','16','13','15') AND pincode = '"+pincode+"'" ;
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.SELECT });
}

module.exports ={
    getPickupsCount,
    getPickups,
    pincodePerformance,
    customerPerformance,
    customerPerformance1,
    pincodePerformance1,
    getPickupData,
    getMobileAndPincodeData,
    getPincodeData
}