const database = require('../config/index');
const pincode_zones = require('../models/pincode_zones');
const pincode_zones_date = require('../models/pincode_zones_date')
const sequelize = require('sequelize')
const landmark = database.landmark;
const moment = require('moment');

const saveData = pincode_zones.saveData = async function (airwaybilno, zone, pickup_id, mobileno, dispatch_status, pay_type, delivery_pincode, email_id, address, product_value, collect_value) {
    //    var query= "INSERT INTO pincode_zones(airwaybilno, zone, pickup_id, mobileno, dispatch_status, pay_type, delivery_pincode, email_id, product_value, collect_value)SELECT * FROM (SELECT '" + airwaybilno + "', '" + zone + "', " + pickup_id + ", '" + mobileno + "', '" + dispatch_status + "', '" + pay_type + "', '" + delivery_pincode + "', '" + email_id + "', '" + product_value + "', '" + collect_value + "') AS tmp WHERE NOT EXISTS ( SELECT pickup_id FROM pincode_zones WHERE pickup_id= "+pickup_id+")";
    var query = "INSERT INTO pincode_zones (airwaybilno, zone, pickup_id, mobileno, dispatch_status, pay_type, delivery_pincode, email_id, address, product_value, collect_value) VALUES ('" + airwaybilno + "', '" + zone + "', " + pickup_id + ", '" + mobileno + "', '" + dispatch_status + "', '" + pay_type + "', '" + delivery_pincode + "', '" + email_id + "', '" + address + "',  '" + product_value + "', '" + collect_value + "')";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.INSERT });
}

const updatePaytypeCOD = pincode_zones.updatePaytypeCOD = async function () {
    var query= "UPDATE pincode_zones SET pay_type = 'COD' WHERE collect_value > 0 AND pay_type != 'COD'";
     return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.INSERT });
}

const updatePaytypePPD = pincode_zones.updatePaytypePPD = async function () {
    var query= "UPDATE pincode_zones SET pay_type = 'PPD' WHERE collect_value <= 0 AND pay_type != 'PPD'";
     return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.INSERT });
}

const createTabelPincodeZones = async function(tableName){
    var query= "CREATE TABLE "+tableName+" (id int NOT NULL AUTO_INCREMENT,tenant_id int,airwaybilno varchar(255), zone varchar(255), pickup_id int, mobileno varchar(255), dispatch_status varchar(255), pay_type varchar(255), delivery_pincode varchar(255), email_id varchar(255), address varchar(500), product_value double(10,2), collect_value double(10,2), PRIMARY KEY (id))";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.CREATE });
}

const saveData1 = pincode_zones_date.saveData = async function (tenant_id,airwaybilno, zone, pickup_id, mobileno, dispatch_status, pay_type, delivery_pincode, email_id, address, product_value, collect_value, tableName) {
    var query = "INSERT INTO "+tableName+" (tenant_id, airwaybilno, zone, pickup_id, mobileno, dispatch_status, pay_type, delivery_pincode, email_id, address, product_value, collect_value) VALUES ("+tenant_id+",'" + airwaybilno + "', '" + zone + "', " + pickup_id + ", '" + mobileno + "', '" + dispatch_status + "', '" + pay_type + "', '" + delivery_pincode + "', '" + email_id + "', '" + address + "',  '" + product_value + "', '" + collect_value + "')";
    return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.INSERT });
}

const updatePaytypeCOD1 = pincode_zones_date.updatePaytypeCOD1 = async function (tableName) {
    var query= "UPDATE "+tableName+" SET pay_type = 'COD' WHERE collect_value > 0 AND pay_type != 'COD'";
     return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.INSERT });
}

const updatePaytypePPD1 = pincode_zones_date.updatePaytypePPD1 = async function (tableName) {
    var query= "UPDATE "+tableName+" SET pay_type = 'PPD' WHERE collect_value <= 0 AND pay_type != 'PPD'";
     return landmark.sequelize.query(query, { type: landmark.sequelize.QueryTypes.INSERT });
}

module.exports ={
    saveData,
    updatePaytypeCOD,
    updatePaytypePPD,
    createTabelPincodeZones,
    saveData1,
    updatePaytypeCOD1,
    updatePaytypePPD1,
}