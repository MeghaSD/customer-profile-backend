var usersModel = require('../models/users');
var trim = require('trim');

//router level middleware for checking existing user.
module.exports.emailExist = function(req,res,next){
  var email = trim(req.body.email)
  usersModel.findOne({'email':email},function(err,result){
      if(err){
          res.json({
              status: 500,
              message: 'Some Error Occured During Email Checking.'
          })
      } else if(result){
          res.json({
              status: 401,
              message : 'Email Already Exist'
          })
      } else{
        next();
      }
    });
  };