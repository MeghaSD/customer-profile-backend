var mongoose = require('mongoose');
const dotenv = require('dotenv');
require('mongoose-double')(mongoose);
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
var conn = mongoose.createConnection(database);
var SchemaTypes = mongoose.Schema.Types;
var AddressSchema = new mongoose.Schema({
    address_label: { type: String, required: true },
    address_value: { type: String, required: true },
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);
module.exports = conn.model('address_masters', AddressSchema);