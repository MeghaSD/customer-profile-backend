module.exports = (sequelize, Sequelize) => {

    const consignor_standard_billing_zones_alias = sequelize.define("consignor_standard_billing_zones_alias", {
    
    zone: {
    
    type: Sequelize.STRING
    
    },
    
    zone_code: {
    
    type: Sequelize.STRING
    
    },
    created_by: {
    
    type: Sequelize.INTEGER
        
    },
    created_datetime:{
        type: Sequelize.DATE
    },
    is_deleted: {
        type: Sequelize.TINYINT
    },
    is_default: {
        type: Sequelize.TINYINT
    },
    is_custom: {
        type: Sequelize.TINYINT
    },
    is_delivery: {
        type: Sequelize.TINYINT
    },
    is_exclude: {
        type: Sequelize.TINYINT
    },
    tat: {
        type: Sequelize.INTEGER
    },
    is_delivery_or_pickup: {
        type: Sequelize.TINYINT
    }
    });
    
    return consignor_standard_billing_zones_alias;
    
    };