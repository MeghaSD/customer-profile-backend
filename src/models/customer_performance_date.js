var mongoose = require('mongoose');
const dotenv = require('dotenv');
require('mongoose-double')(mongoose);
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
const moment = require('moment');
var conn = mongoose.createConnection(database);
var SchemaTypes = mongoose.Schema.Types;
var date = moment(new Date()).format('yyyy_MM_DD');
var cutomerPerformanceSchema = new mongoose.Schema({
    mobileno: { type: String, required: true},
    pincode: { type: String, required: true },
    zone: { type: String, required: true },
    address: { type: String, required: true },
    total_shipments: { type: Number, required: true, default: 0 },
    total_delivered: { type: Number, required: true, default: 0  },
    total_delivered_per: { type: Number, required: true, default: 0  },
    total_cod_shipments: { type: Number, required: true, default: 0  },
    total_cod_delivered: { type: Number, required: true, default: 0  },
    total_cod_delivered_per: { type: Number, required: true, default: 0  },
    total_ppd_shipments: { type: Number, required: true, default: 0  },
    total_ppd_delivered: { type: Number, required: true, default: 0  },
    total_ppd_delivered_per: { type: Number, required: true, default: 0  },
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);
module.exports = conn.model('customer_performances_'+date, cutomerPerformanceSchema);