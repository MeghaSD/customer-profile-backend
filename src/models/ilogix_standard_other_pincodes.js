module.exports = (sequelize, Sequelize) => {

    const ilogix_standard_other_pincodes = sequelize.define("ilogix_standard_other_pincodes", {
    
    pincode: {
    
    type: Sequelize.STRING
    
    },
    
    status: {
    
    type: Sequelize.TINYINT
    
    },
    zone_code: {
    
    type: Sequelize.STRING
        
    },

    is_mapped:{
        type: Sequelize.TINYINT

    },

    is_deleted: {
        type: Sequelize.TINYINT
        
    }
    
    });
    
    
    return ilogix_standard_other_pincodes;
    
    };