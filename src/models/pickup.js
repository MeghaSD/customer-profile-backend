module.exports = (sequelize, Sequelize) => {

    const pickup = sequelize.define("pickup", {
    
    airwaybilno: {
    
    type: Sequelize.STRING
    
    },
    
    orderno: {
    
    type: Sequelize.STRING
    
    },
    consignee_first_name: {
    
    type: Sequelize.INTEGER
        
    },
    consignee_last_name:{
        type: Sequelize.STRING
    },
    consignee_address1: {
        type: Sequelize.TEXT
    },
    consignee_address2: {
        type: Sequelize.TEXT
    },
    destination_city: {
        type: Sequelize.STRING
    },
    pincode: {
        type: Sequelize.STRING
    },
    state: {
        type: Sequelize.STRING
    },
    telephone1: {
        type: Sequelize.STRING
    },
    telephone2: {
        type: Sequelize.STRING
    },
    pickup_location: {
        type: Sequelize.STRING
    },
    product: {
        type: Sequelize.STRING
    },
    item_description: {
        type: Sequelize.INTEGER
    },
    pieces: {
        type: Sequelize.STRING
    },
    order_date: {
        type: Sequelize.DATE
    },
    collectable_value: {
        type: Sequelize.DOUBLE
    },
    product_value: {
        type: Sequelize.DOUBLE
    },
    actual_weight: {
        type: Sequelize.STRING
    },
    volumetric_weight: {
        type: Sequelize.STRING
    },
    import_date: {
        type: Sequelize.DATE
    },
    pickup_date: {
        type: Sequelize.DATE
    },
    vendor: {
        type: Sequelize.STRING
    },
    sub_vendor: {
        type: Sequelize.STRING
    },
    sub_vendor_address1: {
        type: Sequelize.TEXT
    },
    sub_vendor_address2: {
        type: Sequelize.TEXT
    },
    sub_vendor_email: {
        type: Sequelize.STRING
    },
    sub_vendor_phone: {
        type: Sequelize.STRING
    },
    sub_vendor_pincode: {
        type: Sequelize.STRING
    },
    dispatch_status: {
        type: Sequelize.STRING,
        null: '0'
    },
    dispatch_status_code: {
        type: Sequelize.STRING,
        null: '99'
    },
    delivery_date: {
        type: Sequelize.DATE
    },
    length: {
        type: Sequelize.DOUBLE
    },
    breadth: {
        type: Sequelize.DOUBLE
    },
    height: {
        type: Sequelize.DOUBLE
    },
    isvalid: {
        type: Sequelize.TINYINT
    },
    batchno: {
        type: Sequelize.STRING
    },
    weight_update: {
        type: Sequelize.INTEGER
    },
    rtoid: {
        type: Sequelize.STRING
    },
    rp: {
        type: Sequelize.INTEGER
    },
    AirportId: {
        type: Sequelize.STRING
    },

    attempt: {
        type: Sequelize.INTEGER
    },
    verify: {
        type: Sequelize.INTEGER
    },
    rd: {
        type: Sequelize.INTEGER
    },
    first_attempt_date: {
        type: Sequelize.DATE
    },
    last_attempt_date: {
        type: Sequelize.DATE
    },
    exp_delivery: {
        type: Sequelize.DATE
    },
    first_attempt_stat: {
        type: Sequelize.STRING
    },
    status_log_id: {
        type: Sequelize.INTEGER
    },
    is_bulk: {
        type: Sequelize.INTEGER
    },
    invoice_quantity: {
        type: Sequelize.INTEGER
    },
    EnterpriseCode: {
        type: Sequelize.STRING
    },
    log_empid: {
        type: Sequelize.STRING
    },
    rto_flag: {
        type: Sequelize.INTEGER
    },
    OriginServiceArea: {
        type: Sequelize.STRING
    },
    log_updated_date: {
        type: Sequelize.DATE
    },
    Channel_id: {
        type: Sequelize.INTEGER
    },
    sd_rference_no: {
        type: Sequelize.STRING
    },
    transaction_id: {
        type: Sequelize.STRING
    },
    rto_ndr_status: {
        type: Sequelize.STRING
    },
    rto_initiated_date: {
        type: Sequelize.DATE
    },
    rto_intransit_date: {
        type: Sequelize.DATE
    },
    rto_delivery_date: {
        type: Sequelize.DATE
    },
    destination_location: {
        type: Sequelize.STRING
    },
    pickup_location_code: {
        type: Sequelize.STRING
    },
    pickup_location_id: {
        type: Sequelize.INTEGER
    },
    additional_shipment_no: {
        type: Sequelize.STRING
    },
    UrsaSuffixCode: {
        type: Sequelize.STRING
    },
    client_bill_amount: {
        type: Sequelize.DOUBLE,
    },
    client_rto_bill_amount: {
        type: Sequelize.DOUBLE
    },
    client_bill_pay_amount: {
        type: Sequelize.DOUBLE
    },
    client_rto_bill_pay_amount: {
        type: Sequelize.DOUBLE
    },
    client_bill_payment_date: {
        type: Sequelize.DATE
    },
    client_rto_bill_pay_date: {
        type: Sequelize.DATE
    },
    courier_bill_date: {
        type: Sequelize.DATE
    },
    rto_courier_bill_date: {
        type: Sequelize.DATE
    },
    UrsaPrefixCode: {
        type: Sequelize.STRING
    },
    courier_rto_invoice: {
        type :Sequelize.STRING
    },
    courier_bill_amount: {
        type: Sequelize.DOUBLE
    },
    courier_rto_bill_amount: {
        type: Sequelize.DOUBLE
    },
    courier_bill_pay_amount: {
        type: Sequelize.DOUBLE
    },
    courier_rto_bill_pay_amount: {
        type: Sequelize.DOUBLE
    },
    courier_bill_payment_date: {
        type: Sequelize.DATE
    },
    courier_rto_bill_pay_date: {
        type: Sequelize.DATE
    },
    mainvendor_id: {
        type: Sequelize.INTEGER
    },
    subvendor_id: {
        type: Sequelize.INTEGER
    },
    undelivered_date: {
        type: Sequelize.DATE
    },
    courier_id: {
        type: Sequelize.INTEGER
    },
    created_datetime: {
        type: Sequelize.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
        allowNull: false
    },
    sku: {
        type: Sequelize.STRING
    },
    rto_vendor_code: {
        type: Sequelize.STRING
    },
    rto_sub_vendor_name: {
        type: Sequelize.STRING
    },
    rto_sub_vendor_address: {
        type: Sequelize.TEXT
    },
    rto_sub_vendor_tel: {
        type: Sequelize.STRING
    },
    rto_sub_vendor_city: {
        type: Sequelize.STRING
    },
    rto_sub_vendor_pincode: {
        type: Sequelize.STRING
    },
    invoice_date: {
        type: Sequelize.DATE
    },
    item_category: {
        type: Sequelize.STRING
    },
    vendor_gstin: {
        type: Sequelize.STRING
    },
    product_gst_hsn: {
        type: Sequelize.STRING
    },
    total_sale_value: {
        type: Sequelize.DOUBLE
    },
    tax_cgst: {
        type: Sequelize.DOUBLE
    },
    tax_igst: {
        type: Sequelize.DOUBLE
    },
    tax_sgst: {
        type: Sequelize.DOUBLE
    },
    tax_ugst: {
        type: Sequelize.DOUBLE
    },
    business_criteria_code: {
        type: Sequelize.STRING
    },
    brand: {
        type: Sequelize.STRING
    },
    lsp_token_number: {
        type: Sequelize.STRING
    },
    recommended_lsp_id: {
        type: Sequelize.INTEGER
    },
    recommended_lsp_list: {
        type: Sequelize.STRING
    },
    confirmation_flag: {
        type: Sequelize.TINYINT
    },
    cancelled_date: {
        type: Sequelize.DATE
    },
    shopify_client_id: {
        type: Sequelize.TINYINT
    },
    consignee_email: {
        type: Sequelize.STRING
    },
    carrier_name: {
        type: Sequelize.STRING
    },
    destination_area: {
        type: Sequelize.STRING
    },
    shipping_charges: {
        type: Sequelize.DOUBLE
    },
    discount: {
        type: Sequelize.DOUBLE
    },
    track_id: {
        type: Sequelize.INTEGER
    },
    invoice_no: {
        type: Sequelize.STRING
    },
    CODReturnID: {
        type: Sequelize.STRING
    },
    CODFormId: {
        type: Sequelize.STRING
    },
    CODTrackingNumber: {
        type: Sequelize.STRING
    },
    FormId: {
        type: Sequelize.STRING
    },
    additional_attribute1: {
        type: Sequelize.STRING
    },
    additional_attribute2: {
        type: Sequelize.STRING
    },
    StringBarcodes: {
        type: Sequelize.STRING
    },
    old_pickup_id: {
        type: Sequelize.INTEGER
    }
    
    
    });
    
    return pickup;
    
    };