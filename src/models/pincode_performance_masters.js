var mongoose = require('mongoose');
const dotenv = require('dotenv');
require('mongoose-double')(mongoose);
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
var conn = mongoose.createConnection(database);
var SchemaTypes = mongoose.Schema.Types;
var pincodePerformanceSchema = new mongoose.Schema({
    pincode_label: { type: String, required: true },
    pincode_value: { type: String, required: true },
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);
module.exports = conn.model('pincode_performance_masters', pincodePerformanceSchema);