
module.exports = (sequelize, Sequelize) => {

    const Pincode_zones = sequelize.define("pincode_zones", {
    
    airwaybilno: {
    
    type: Sequelize.STRING
    
    },
    
    zone: {
    
    type: Sequelize.STRING
    
    },
    pickup_id: {
    
    type: Sequelize.INTEGER,
    unique: true
        
    },

    mobileno:{
        type: Sequelize.STRING
    },
    dispatch_status: {
        type: Sequelize.STRING
    },

    pay_type: {
        type: Sequelize.STRING
    },

    delivery_pincode: {
        type: Sequelize.STRING
    },

    email_id: {
        type: Sequelize.STRING
    },

    address: {
        type: Sequelize.STRING
    },

    product_value: {
        type: Sequelize.DOUBLE
    },

    collect_value: {
        type: Sequelize.DOUBLE
    }
    
    
    });
    
    
    return Pincode_zones;
    
    };