var mongoose = require('mongoose');
const dotenv = require('dotenv');
require('mongoose-double')(mongoose);
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
var conn = mongoose.createConnection(database);
var SchemaTypes = mongoose.Schema.Types;
var recommendationSchema = new mongoose.Schema({
    customer_type: { type: String, required: true },
    customer_history: { type: String, required: true },
    zone: { type: String, required: true },
    address: { type: String, default: '' },
    pincode_performance: { type: String, default: '' },
    order_history: { type: String, default: '' },
    order_history_greater: { type: String, default: 0},
    pay_type: { type: String, required: true },
    order_status: { type: String, default: '' },
    delivery_possibility: { type: String, required: true },
    flag: { type: String, required: true },
    address_length: { type: Number, default: 0},
    pincode_greater: { type: Number, default: 0},
    pincode_less: { type: Number, default: 0},
    status_greater: { type: Number, default: 0},
    status_less: { type: Number, default: 0}
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);
module.exports = conn.model('recommendation_config', recommendationSchema);