var mongoose = require('mongoose');
const validator = require("validator");
const bcrypt = require("bcryptjs");
const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI
var conn = mongoose.createConnection(database);
var usersSchema = new mongoose.Schema({
    userName: { type: String, required: true},
    email: {
        type: String,
        required: [true, "Please fill your email"], unique: true,
        lowercase: true,
        validate: [validator.isEmail, " Please provide a valid email"],
    },
    password: {
        type: String,
        required: [true, "Please fill your password"],
        minLength: 6,
        select: false,
      },
    passwordHash : {type:String, required : true},
    salt : {type:String, required: true},
},{
    versionKey: false // You should be aware of the outcome after set to false (_v column not generate)
}
);

module.exports = conn.model('users', usersSchema);