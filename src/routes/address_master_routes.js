const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const addressController = require('../controllers/addressController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Address Masters Informations (Date: 18-04-2022)
*/
router.post('/saveAddress', addressController.saveAddress);

/*
    Get address masters all data (Date: 21-04-2022)
 */
router.get('/getAddressMasters', addressController.getAddressMasters)

    /*
    Get customer types all data (Date: 21-04-2022)
 */
router.get('/getCustomerTypes', addressController.getCustomerTypes)

/*
    Get customer history all data (Date: 21-04-2022)
 */
router.get('/getCustomerHistory', addressController.getCustomerHistory)

/*
    Get zones all data (Date: 21-04-2022)
 */
router.get('/getZones', addressController.getZones)

module.exports = router;