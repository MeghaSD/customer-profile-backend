const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const authController = require('../controllers/authController');

/*
    testing api
*/
router.get('/demoTesting', (req, res) => {
    res.json({
        status: 200,
        message: "demo testing router success"
    })
});


/*
    Use this api for login user  (Date: 26-02-2022)
*/
router.post('/login', authController.login);

module.exports = router;