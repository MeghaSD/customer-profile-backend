const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const deliveryPossibilityController = require('../controllers/deliveryPossibilityController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Delivery Possibility Masters Informations (Date: 18-04-2022)
*/
router.post('/saveDeliveryPossibility', deliveryPossibilityController.saveDeliveryPossibility);


/*
    Get delivery possibility masters all data (Date: 21-04-2022)
*/
router.get('/getDeliveryMasters', deliveryPossibilityController.getDeliveryMasters)


module.exports = router;