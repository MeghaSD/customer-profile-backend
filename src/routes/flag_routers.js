const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const flagController = require('../controllers/flagController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Flag Masters Informations (Date: 18-04-2022)
*/
router.post('/saveFlag', flagController.saveFlag);

/*
    Get flag masters all data (Date: 21-04-2022)
*/
router.get('/getFlagMasters', flagController.getFlagMasters)

module.exports = router;