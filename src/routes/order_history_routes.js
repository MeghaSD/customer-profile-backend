const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const orderHistoryController = require('../controllers/orderHistoryController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Order History Masters Informations (Date: 18-04-2022)
*/
router.post('/saveOrderHistory', orderHistoryController.saveOrderHistory);

/*
    Get order history masters all data (Date: 21-04-2022)
*/
router.get('/getOrdersHistoryMasters', orderHistoryController.getOrdersHistoryMasters)

module.exports = router;