const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const orderStatusController = require('../controllers/orderStatusController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Order Status Masters Informations (Date: 18-04-2022)
*/
router.post('/saveOrderStatus', orderStatusController.saveOrderStatus);

/*
    Get order status masters all data (Date: 21-04-2022)
*/
router.get('/getOrdersStatusMasters', orderStatusController.getOrdersStatusMasters)

module.exports = router;