const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const paytypeController = require('../controllers/paytypeController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save PayType Masters Informations (Date: 18-04-2022)
*/
router.post('/savePayType', paytypeController.savePayType);

/*
    Get pay_type masters all data (Date: 21-04-2022)
*/
router.get('/getPaytypeMasters', paytypeController.getPaytypeMasters)

module.exports = router;