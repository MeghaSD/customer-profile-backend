const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const pincodePerformanceController = require('../controllers/pincodePerformanceController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Pincode Performance Masters Informations (Date: 18-04-2022)
*/
router.post('/savePincodePerformance', pincodePerformanceController.savePincodePerformance);

/*
    Get pincode performance masters all data (Date: 21-04-2022)
 */
router.get('/getPincodeMasters', pincodePerformanceController.getPincodeMasters)

module.exports = router;