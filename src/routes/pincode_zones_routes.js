const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
var pincodeZonesController = require('../controllers/pincodeZonesController')
const cron = require('node-cron');
const pincode_zones = require("../dao/pincode_zones_dao");
const moment = require('moment');
const pickupDao = require("../dao/pickup_dao");
const commanController = require("../controllers/commanController");
const dotenv = require('dotenv');
var currentPath = process.cwd();
dotenv.config({path:currentPath +'/.env'});
const database = process.env.MONGO_URI;
var MongoClient = require('mongodb').MongoClient;
const trim = require("trim");
const pincode_performance_date_model = require("../models/pincode_performance_date");
const customer_performance_date_model = require("../models/customer_performance_date");


router.get('/savePincodeZones', pincodeZonesController.savePincodeZones);

router.get('/savePincodePerformanceData', pincodeZonesController.savePincodePerformanceData);

router.get('/saveCustomerPerformance', pincodeZonesController.saveCustomerPerformance);

router.get('/getPincodePerformance', pincodeZonesController.getPincodePerformance);

router.post('/getCustomerProfile', pincodeZonesController.getCustomerProfile)

router.post('/getCustomerProfile2', pincodeZonesController.getCustomerProfile2)

router.post('/getCustomerProfileData', pincodeZonesController.getCustomerProfileData)

cron.schedule('00 00 01 * * 0-6', async () => {
  console.log("running a task save pincode zones")
  var date = moment(new Date()).format('yyyy_MM_DD');
  var tableName = 'pincode_zones_' + date
  console.log("tableName---", tableName)

  const createPincodeZones = await pincode_zones.createTabelPincodeZones(tableName)
  console.log("createPincodeZones---", createPincodeZones)
  if (createPincodeZones) {
    try {
      var total = [];
      total = await pickupDao.getPickupsCount(tableName)
      var totalPickup = total[0]['count']
      console.log("totalPickup---", totalPickup)
      var number = 0;
      for (let count = 0; count <= totalPickup; count = count + 1000) {
        console.log("count---", count)
        await pickupDao.getPickups(tableName, limit = 1000).then(async data => {
          number = number + data.length
          console.log("number---", number)
          if (data) {
            for await (let element of data) {
              await commanController.getZonesData(element.sub_vendor_pincode, element.pincode, element.airwaybilno, element.id, element.telephone1, element.dispatch_status, element.product, element.consignee_email, element.consignee_address1, element.product_value, element.collectable_value).then(async function (pincode_zones_data) {
                // console.log("getzones---",pincode_zones_data)
                if (pincode_zones_data != '' && pincode_zones_data != undefined) {
                  for await (let pincode of pincode_zones_data) {
                    await pincode_zones.saveData1(tenant_id = 1198, pincode.airwaybilno, pincode.zone, pincode.pickup_id, pincode.mobileno, pincode.dispatch_status, pincode.pay_type, pincode.delivery_pincode, pincode.email_id, pincode.address, pincode.product_value, pincode.collect_value, tableName).then(async function (save) {
                      if (save) {
                        console.log("save successfully")
                      } else {
                        console.log("not save")
                      }

                    })
                  }
                }
              })
            }
            await pincode_zones.updatePaytypeCOD1(tableName).then(function (updateCOD) {
              if (updateCOD) {
                console.log("update pay_type COD")
              } else {
                console.log("no data ")
              }
            })
            await pincode_zones.updatePaytypePPD1(tableName).then(function (updatePPD) {
              if (updatePPD) {
                console.log("update pay_type PPD")
              } else {
                console.log("no data ")
              }
            })
            console.log("datalength--", data.length)
          } else {
            console.log("No Data in pickup table")
          }
          // res.send(data)
        })
      }
    } catch (error) {
      res.status(500).send({
        message:
          error.message || "Some error occurred while creating pincode_zones."
      });
    }
  } else {
    console.log("else part ")
  }
})

cron.schedule('00 00 03 * * 0-6', async () => {
  console.log("running a task save customer performance")
  var date = moment(new Date()).format('yyyy_MM_DD');
  var tableName = 'pincode_zones_' + date
  console.log("tableName---", tableName)
  // MongoClient.connect(database, async function (err, db) {
  //   if (err) throw err;
  //   var dbase = db.db("smart_booking");
  //   var collectionName = 'customer_performances_' + date;
  //   const createCustomerPerformance = await dbase.createCollection(collectionName)
  //   console.log("createCustomerPerformance---", createCustomerPerformance)
  //   let customer_performance = dbase.collection(collectionName);
  //   if (createCustomerPerformance) {
      const getData = await pincodeZonesController.saveCronWiseCustomerPerformance(tableName)
      console.log("getData---", getData.length)
      for await (let pincode of getData) {
        customer_performance_date_model.findOneAndUpdate(
          {
            mobileno: trim(pincode.mobileno),
            pincode: trim(pincode.delivery_pincode),
            zone: trim(pincode.zone),
            address: trim(pincode.address)
          },
          {
            $set: {
              total_shipments: pincode.total_shipments,
              total_delivered: pincode.totalDelivered,
              total_delivered_per: Math.round(pincode.totalDeliveredPer),
              total_cod_shipments: pincode.totalCOD,
              total_cod_delivered: pincode.totalCODDelivered,
              total_cod_delivered_per: Math.round(pincode.totalCODDeliveredPer),
              total_ppd_shipments: pincode.totalPPD,
              total_ppd_delivered: pincode.totalPPDDelivered,
              total_ppd_delivered_per: Math.round(pincode.totalPPDDeliveredPer),
            }
          },
          { upsert: true, new: true },
          function (err, customer_performance) {
            if (err) {
              console.log("err---")
            } else {
              // console.log("customer performance save successfully")
            }
          });
      }

  //   } else {
  //     console.log("reached on else part...")
  //   }
  // })

})

cron.schedule('00 00 05 * * 0-6', async () => {
  console.log("running a task save pincode performance")
  var date = moment(new Date()).format('yyyy_MM_DD');
  var tableName = 'pincode_zones_' + date
  console.log("tableName---", tableName)
//   MongoClient.connect(database, async function (err, db) {
    // if (err) throw err;
    // var dbase = db.db("smart_booking");
    // var collectionName = 'pincode_performances_' + date;
    // const createPincodePerformance = await dbase.createCollection(collectionName)
    // console.log("createPincodePerformance---", createPincodePerformance)
    // let pincode_performance = dbase.collection(collectionName);
    // if (createPincodePerformance) {
      const getData = await pincodeZonesController.saveCronWisePincodePerformanceData(tableName)
      console.log("getData---", getData.length)
      for await (let pincode of getData) {
        pincode_performance_date_model.findOneAndUpdate(
          {
            pincode: trim(pincode.delivery_pincode),
            zone: trim(pincode.zone)
          },
          {
            $set: {
              total_shipments: pincode.total_shipments,
              total_delivered: pincode.totalDelivered,
              total_delivered_per: Math.round(pincode.totalDeliveredPer),
              total_cod_shipments: pincode.totalCOD,
              total_cod_delivered: pincode.totalCODDelivered,
              total_cod_delivered_per: Math.round(pincode.totalCODDeliveredPer),
              total_ppd_shipments: pincode.totalPPD,
              total_ppd_delivered: pincode.totalPPDDelivered,
              total_ppd_delivered_per: Math.round(pincode.totalPPDDeliveredPer),
            }
          },
          { upsert: true, new: true },
          function (err, pincode_performance) {
            if (err) {
              console.log("err---")
            } else {
              // console.log("pincode performance save successfully")
            }
          });
      }
      console.log("done success")
    // } else {
    //   console.log("reached on else part..")
    // }
//   })
})

module.exports = router;