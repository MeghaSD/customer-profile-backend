const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
const recommendationController = require('../controllers/recommendationController');
const authController = require('../controllers/authController');

// Protect all routes after this middleware (Date: 26-02-2022)
// router.use(authController.protect);

/*
    Save Recommendation Config (Date: 18-04-2022)
*/
router.post('/saveRecommendation', recommendationController.saveRecommendation);

/*
    Update api For Recommendation Config (Date: 19-04-2022)
*/
router.put('/updateRecommendation', recommendationController.updateRecommendation);

/*
    Get recommendation config all data (Date: 22-04-2022)
*/
router.get('/getRecommendationConfig', recommendationController.getRecommendationConfig)

/*
    Get recommendation config data required id wise (Date: 22-04-2022)
*/
router.get('/getRecommendation', recommendationController.getRecommendation)

/*
    Delete recommendation config api (Date: 23-04-2022)
*/
router.delete('/deleteRecommendation', recommendationController.deleteRecommendation)
    

module.exports = router;