const express = require('express');
const mongoose = require('mongoose');
var router = express.Router();
var validator = require('../middleware/validator');
const userController = require('../controllers/userController');

/*
    Save Client Informations (Date: 25-02-2022)
*/
router.post('/saveUser', validator.emailExist,userController.saveUser);


module.exports = router;